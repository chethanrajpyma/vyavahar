<!doctype html>
<html class="no-js" lang="">
    <meta charset="utf-8">
    <title>Vyavahar - Seller</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Include Table Fixed Header CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">
</head>

<body>
    
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default" style="background-color: #ffffff;border-color: #ffffff;">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">

                      <li class="active">
                        <a style="color: black" href="<?php echo base_url('Dashboard');?>" ><i class="fa fa-home fa-fw fa-2x"></i>Dashboard</a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Orders');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Orders </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Packing Materials </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Returns </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Products');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Products </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Inventory');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Inventory </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Statement');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Statement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="#"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Invoice </a>
                    </li>

                    <li class="">
                        <a style="color: black" href=""><i class="fa fa-product-hunt fa-fw fa-2x"></i>Advertisement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Logs');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Logs </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Users');?>"><i class="fa fa-user-plus fa-fw fa-2x"></i>Users </a>
                    </li>   
                </ul>
            </div>
        </nav>
    </aside>

    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="img-responsive navbar-brand" href="./"><img src="<?php echo base_url(); ?>assets/images/profile.jpeg" alt="Logo"></a>
                    <a class="img-responsive navbar-brand hidden" href="./"><img src="<?php echo base_url(); ?>assets/images/profile.jpeg" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">

                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="img-responsive user-avatar rounded-circle" src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?php echo base_url('Profile');?>"><i class="fa fa- user"></i>My Profile</a>

                            <a class="nav-link" href="<?php echo base_url('Login/Logout');?>"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <!-- Content -->
        <div class="content">

            <!--Div to show top error message-->
            <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>
            <!--Div to show top error message-->
            <div id="result"><?php if($this->session->flashdata('result')){echo $this->session->flashdata('result');} ?></div>
            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>

            <div class="row">
                <div class="col-sm-12">
                    <h4 style="font-weight: 600"><?php echo $title;?></h4>
                </div>

                <div class="visible-sm"><br></div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body card-block">
                        <!--form open-->
                        <?php echo form_open_multipart(base_url('Seller/SaveSellerMaster'), array('id'=>'submitForm')); ?>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label>Company Name<span style="color: red;font-size: 20px;">*</span></label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="cname" name="cname" value="<?php if($this->session->flashdata('fcname')){ echo $this->session->flashdata('fcname');}?>" placeholder="Name of of the Company" class="AlphaNumericSpaces form-control Char128">
                                    <small><span class="error_txt" id="error_cname"><?php if($this->session->flashdata('error_cname')){echo $this->session->flashdata('error_cname');} ?></span></small>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label>Name of the  Seller<span style="color: red;font-size: 20px;">*</span></label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="sname" name="sname" value="<?php if($this->session->flashdata('fsname')){ echo $this->session->flashdata('fsname');}?>" placeholder="Name of Seller" class="AlphaNumericSpaces form-control Char128">
                                    <small><span class="error_txt" id="error_sname"><?php if($this->session->flashdata('error_sname')){echo $this->session->flashdata('error_sname');} ?></span></small>
                                </div>
                            </div>
                            
                             <div class="row form-group">
                                <div class="col col-md-3">
                                    <label>Buissness Established</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="buissness" name="buissness" value="<?php if($this->session->flashdata('fbuissness')){ echo $this->session->flashdata('fbuissness');}?>" placeholder="Ex. 2020" class="form-control IntegerOnly Char4">
                                    <small><span class="error_txt" id="error_buissness"><?php if($this->session->flashdata('error_buissness')){echo $this->session->flashdata('error_buissness');} ?></span></small>
                                </div>
                            </div> 

                            <!-- Phone Field -->
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label>Phone Number </label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" id="phone" value="<?php if($this->session->flashdata('fphone')){ echo $this->session->flashdata('fphone');}?>" name="phone" class="form-control IntegerOnly" placeholder="Phone Number" maxlength= "12">
                                            <small><span class="error_txt" id="error_phone"><?php if($this->session->flashdata('error_phone')){echo $this->session->flashdata('error_phone');} ?></span></small>
                                        </div>
                                        <div class="visible-xs"><br></div>
                                        <div class="col-sm-6">
                                            <input type="text" id="mobile" value="<?php if($this->session->flashdata('fmobile')){ echo $this->session->flashdata('fmobile');}?>" name="mobile" class="form-control IntegerOnly" placeholder="Mobile Number" maxlength= "12">
                                            <small><span class="error_txt" id="error_mobile"><?php if($this->session->flashdata('error_mobile')){echo $this->session->flashdata('error_mobile');} ?></span></small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Email field -->
                             <div class="row form-group">
                                <div class="col-sm-3">
                                    <label>Email <span style="color: red;font-size: 20px;"></span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="email" name="email" value="<?php if($this->session->flashdata('femail')){ echo $this->session->flashdata('femail');}?>" class="form-control" placeholder="Enter email address">
                                    <small><span class="error_txt" id="error_email"><?php if($this->session->flashdata('error_email')){echo $this->session->flashdata('error_email');} ?></span></small>
                                </div>
                            </div>
 
                            <!-- Uses Field -->
                           <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Product Category</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control input-sm" id="pcateg" name="pcateg">
                                           <option value selected>--Select Product Category --</option>
                                            <option value="1" <?php if($this->session->flashdata('fpcateg')=='1'){echo 'selected';}?>>Beauty</option >
                                            <option value="2" <?php if($this->session->flashdata('fpcateg')=='2'){echo 'selected';}?>>Books</option>
                                            <option value="3" <?php if($this->session->flashdata('fpcateg')=='3'){echo 'selected';}?>>Camera & Photo </option>
                                            <option value="4" <?php if($this->session->flashdata('fpcateg')=='4'){echo 'selected';}?>>Cell Phones & Accessories</option>
                                            <option value="5" <?php if($this->session->flashdata('fpcateg')=='5'){echo 'selected';}?>>Office Products</option>
                                            <option value="6" <?php if($this->session->flashdata('fpcateg')=='6'){echo 'selected';}?>>Sports</option> 
                                        </select>
                                        <small><span class="error_txt" id="error_pcateg"><?php if($this->session->flashdata('error_pcateg')){echo $this->session->flashdata('error_pcateg');} ?></span></small>
                                    </div>
                                </div>
                            </div>

                              <!-- Address Field -->
                             <div class="row form-group">
                                <div class="col-sm-3">
                                    <label>Address <span style="color: red;font-size: 20px;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" id="address1" value="<?php if($this->session->flashdata('faddress1')){ echo $this->session->flashdata('faddress1');}?>" name="address1" class="form-control AlphaNumericSpaces Char128" placeholder="Address line 1">
                                            <small><span class="error_txt" id="error_address1"><?php if($this->session->flashdata('error_address1')){echo $this->session->flashdata('error_address1');} ?></span></small>
                                        </div>
                                        <div class="visible-xs"><br></div>
                                        <div class="col-sm-6">
                                            <input type="text" id="address2" value="<?php if($this->session->flashdata('faddress2')){ echo $this->session->flashdata('faddress2');}?>" name="address2" class="form-control AlphaNumericSpaces Char128" placeholder="Address line 2">
                                            <small><span class="error_txt" id="error_address2"><?php if($this->session->flashdata('error_address2')){echo $this->session->flashdata('error_address2');} ?></span></small>
                                        </div>
                                    </div>
                                </div>
                            </div> 

                            <!-- city field -->
                           <div class="row form-group">
                                <div class="col-sm-3">
                                    <label>City <span style="color: red;font-size: 20px;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="city" name="city" value="<?php if($this->session->flashdata('fcity')){ echo $this->session->flashdata('fcity');}?>" class="AlphaNumeric form-control " placeholder="Enter city name" maxlength= "32">
                                    <small><span class="error_txt" id="error_city"><?php if($this->session->flashdata('error_city')){echo $this->session->flashdata('error_city');} ?></span></small>
                                </div>
                            </div>

                            <!-- state field -->
                         <div class="row form-group">
                                <div class="col-sm-3">
                                    <label>State <span style="color: red;font-size: 20px;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="state" name="state" value="<?php if($this->session->flashdata('fstate')){ echo $this->session->flashdata('fstate');}?>" class="AlphaNumericSpaces form-control Char16 " placeholder="Enter State Name" maxlength= "32">
                                    <small><span class="error_txt" id="error_state"><?php if($this->session->flashdata('error_state')){echo $this->session->flashdata('error_state');} ?></span></small>
                                </div>
                            </div> 

                            <!-- country field -->
                            <div class="row form-group">
                                <div class="col-sm-3">
                                    <label>Country <span style="color: red;font-size: 20px;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="country" name="country" value="<?php if($this->session->flashdata('fcountry')){ echo $this->session->flashdata('fcountry');}?>" class="AlphaNumericSpaces form-control Char16 " placeholder="Enter State Name" maxlength= "32">
                                    <small><span class="error_txt" id="error_country"><?php if($this->session->flashdata('error_country')){echo $this->session->flashdata('error_country');} ?></span></small>
                                </div>
                            </div> 

                            <!-- postal field -->
                            <!-- <div class="row form-group">
                                <div class="col-sm-3">
                                    <label>Pincode </label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="pincode" name="pincode" value="<?php if($this->session->flashdata('fpincode')){ echo $this->session->flashdata('fpincode');}?>" class="Char6 IntegerOnly form-control" placeholder="Postal Code" maxlength= "6">
                                    <small><span class="error_txt" id="error_pincode"><?php if($this->session->flashdata('error_pincode')){echo $this->session->flashdata('error_pincode');} ?></span></small>
                                </div>
                            </div>  

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body card-block">


                            <!-- Spl Image Field -->
                            <!--  <div class="row form-group">
                                <div class="col col-md-3">
                                    <label> Pancard  <span style="color: red;font-size: 20px;">*</span></label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="file" id="pancard" name="pancard" placeholder="Text" class="form-control">
                                    <small><p><i>Supported: PNG / JPG, Size : Max 2mb<br><b style="color: red">Image dimension should be 300 * 200 pixels</b></i></p></small>
                                    <small><span class="error_txt" id="error_pancard"><?php if($this->session->flashdata('error_pancard')){echo $this->session->flashdata('error_pancard');} ?></span></small>
                                </div>
                            </div>  -->

                            <!-- Banner Image Field -->
                            <!-- <div class="row form-group">
                                <div class="col col-md-3">
                                    <label> Company ID  <span style="color: red;font-size: 20px;">*</span></label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="file" id="companyid" name="companyid" placeholder="Text" class="form-control">
                                    <small><p><i>Supported: PNG / JPG, Size : Max 2mb<br><b style="color: red">Image dimension should be 600*400 pixels</b></i></p></small>
                                    <small><span class="error_txt" id="error_companyid"><?php if($this->session->flashdata('error_companyid')){echo $this->session->flashdata('error_companyid');} ?></span></small>
                                </div>
                            </div>  -->

                            <!-- Banner Image Field -->
                            <!--  <div class="row form-group">
                                <div class="col col-md-3">
                                    <label> Company GST  <span style="color: red;font-size: 20px;">*</span></label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="file" id="gst" name="gst" placeholder="Text" class="form-control">
                                    <small><p><i>Supported: PNG / JPG, Size : Max 2mb<br><b style="color: red">Image dimension should be 600*400 pixels</b></i></p></small>
                                    <small><span class="error_txt" id="error_gst"><?php if($this->session->flashdata('error_gst')){echo $this->session->flashdata('error_gst');} ?></span></small>
                                </div>
                            </div>  -->

                            <div class="row">
                                <div class="col-sm-12">

                                    <!-- Cancel Button -->
                                    <a href="<?php echo base_url('Seller/SellerMaster')?>"><button type="button" id="canceluser" class="btn btn-default pull-right btn-sm"><span class="fa fa-times"></span> Cancel</button></a>
                                    <span class="pull-right">&nbsp;</span>

                                    <!-- Save Button -->
                                    <button type="submit" name="submitForm" id="submitForm" class="btn btn-success btn-sm pull-right"><span class="fa fa-save"></span> Save</button>
                                </div>
                            </div>

                            <?php echo form_close() ?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Include Table Fixed Header CSS -->
            <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">   
        </div>
        <!-- /.content -->
        <div class="clearfix"></div>
        <!-- Footer -->
    </div>
    
    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script> var base_url='<?php echo base_url(); ?>'; </script>
    
    <script>
        $(document).ready(function(){

            //Accepts only alpha numeric and spaces
            $(".AlphaNumericSpaces").keypress(function(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode == 32 || (charCode>=48 && charCode<=57) || (charCode>=65 && charCode<=90) || (charCode>=97 && charCode<=122)){
                    return true;
                }
                return false;
            }); 

            //Accepts only alpha numeric 
            $(".AlphaNumeric").keypress(function(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if ((charCode>=48 && charCode<=57) || (charCode>=65 && charCode<=90) || (charCode>=97 && charCode<=122)){
                    return true;
                }
                return false;
            });
            
            $('#phone').keyup(function() {
                var val = this.value.replace(/\D/g, '');
                var newVal = '';
                for(i=0;i<2;i++){
                 if (val.length > 5) {
                  newVal += val.substr(0, 5) + ' ';
                  val = val.substr(5);
                }
                }
                newVal += val;
                this.value = newVal;
            });

            $('#mobile').keyup(function() {
                var val = this.value.replace(/\D/g, '');
                var newVal = '';
                for(i=0;i<2;i++){
                 if (val.length > 5) {
                  newVal += val.substr(0, 5) + ' ';
                  val = val.substr(5);
                }
                }
                newVal += val;
                this.value = newVal;
            });

            $('#tsphone').keyup(function() {
                var val = this.value.replace(/\D/g, '');
                var newVal = '';
                for(i=0;i<2;i++){
                 if (val.length > 5) {
                  newVal += val.substr(0, 5) + ' ';
                  val = val.substr(5);
                }
                }
                newVal += val;
                this.value = newVal;
            });
         
        });
    </script>

</body>
</html>
