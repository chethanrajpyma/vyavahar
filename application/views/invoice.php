 <!doctype html>
<html class="no-js" lang="">
    <meta charset="utf-8">
    <title>Vyavahar -  Logs</title>

    <link rel="icon" href="<?php echo base_url(); ?>assets/images/logo.jpeg">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Include Table Fixed Header CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">

    <!-- ----------------------------------------------------------------- -->
    <!-- Later need to add in other folder -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- ----------------------------------------------------------------- -->

    <style type="text/css">
        .SearchBox {
            width: 100%;
            height: 40px;
            padding: 0px 10px;
            margin-bottom: 10px;
            border: none;
            font-family: 'Open Sans', sans-serif;
        }

        #invoice{
    padding: 30px;
}

/*.invoice {
    position: relative;
    background-color: #FFF;
    min-height: 680px;
    padding: 15px
}*/

.invoice header {
    padding: 10px 0;
    margin-bottom: 20px;
    border-bottom: 1px solid #3989c6
}

.invoice .company-details {
    text-align: right
}

.invoice .company-details .name {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .contacts {
    margin-bottom: 20px
}

.invoice .invoice-to {
    text-align: left
}

.invoice .invoice-to .to {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .invoice-details {
    text-align: right
}

.invoice .invoice-details .invoice-id {
    margin-top: 0;
    color: #3989c6
}

.invoice main {
    padding-bottom: 50px
}

.invoice main .thanks {
    margin-top: -100px;
    font-size: 2em;
    margin-bottom: 50px
}

.invoice main .notices {
    padding-left: 6px;
    border-left: 6px solid #3989c6
}

.invoice main .notices .notice {
    font-size: 1.2em
}

.invoice table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px
}

.invoice table td,.invoice table th {
    padding: 15px;
    background: #eee;
    border-bottom: 1px solid #fff
}

.invoice table th {
    white-space: nowrap;
    font-weight: 400;
    font-size: 16px
}

.invoice table td h3 {
    margin: 0;
    font-weight: 400;
    color: #3989c6;
    font-size: 1.2em
}

.invoice table .qty,.invoice table .total,.invoice table .unit {
    text-align: right;
    font-size: 1.2em
}

.invoice table .no {
    color: #fff;
    font-size: 1.6em;
    background: #3989c6
}

.invoice table .unit {
    background: #ddd
}

.invoice table .total {
    background: #3989c6;
    color: #fff
}

.invoice table tbody tr:last-child td {
    border: none
}

.invoice table tfoot td {
    background: 0 0;
    border-bottom: none;
    white-space: nowrap;
    text-align: right;
    padding: 10px 20px;
    font-size: 1.2em;
    border-top: 1px solid #aaa
}

.invoice table tfoot tr:first-child td {
    border-top: none
}

.invoice table tfoot tr:last-child td {
    color: #3989c6;
    font-size: 1.4em;
    border-top: 1px solid #3989c6
}

.invoice table tfoot tr td:first-child {
    border: none
}

/*.invoice footer {
    width: 100%;
    text-align: center;
    color: #777;
    border-top: 1px solid #aaa;
    padding: 8px 0
}*/

@media print {
    .invoice {
        font-size: 11px!important;
        overflow: hidden!important
    }

    .invoice footer {
        position: absolute;
        bottom: 10px;
        page-break-after: always
    }

    .invoice>div:last-child {
        page-break-before: always
    }
}
    </style>
</head>

<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default" style="background-color: #ffffff;border-color: #ffffff;">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">

                  <li class="active">
                        <a style="color: black" href="<?php echo base_url('Dashboard');?>" ><i class="fa fa-home fa-fw fa-2x"></i>Dashboard</a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Orders');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Orders </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Packing Materials </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Returns </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Products');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Products </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Inventory');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Inventory </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Statement');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Statement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Invoice');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Invoice </a>
                    </li>

                    <li class="">
                        <a style="color: black" href=""><i class="fa fa-product-hunt fa-fw fa-2x"></i>Advertisement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Logs');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Logs </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Users');?>"><i class="fa fa-user-plus fa-fw fa-2x"></i>Users </a>
                    </li>   
                </ul>
            </div>
        </nav>
    </aside>
    <!-- /#left-panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                     <a class="img-responsive navbar-brand" href="#"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo"></a> 

                    <a class="img-responsive navbar-brand hidden" href="./"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo" width="50%"></a>

                        <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>

                </div>

            </div>

            <div class="top-right">
                <div class="header-menu">

                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="img-responsive user-avatar rounded-circle" src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo">
                        </a>


                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?php echo base_url('Profile');?>"><i class="fa fa- user"></i>My Profile</a>

                            <a class="nav-link" href="<?php echo base_url('Login/Logout');?>"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <!-- Content -->
        <div class="content">

            <!--Div to show top error message-->
            <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>

            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>

            <div class="row">
                <div class="col-sm-4">
                    <h4 style="font-weight: 600"><?php echo $title;?></h4>
                </div>

                <div class="visible-sm"><br></div>

                <div class="visible-sm"><br></div>

                <div class="col-sm-4">
                    <input id="SearchBox" type="text" name="search" class="SearchBox " placeholder="Search..">
                </div>
                
            </div>

            <section>
                <div style="width:100%; height:55vh; border-top:3px SOLID LIGHTGREY; overflow-y:scroll; overflow-x:scroll;">
                    <table id="CommonTable" class="TblFont">
                        <thead class="hidden-xs hidden-sm">
                          <div id="invoice">

    <div class="toolbar hidden-print">
        <div class="text-right">
            <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
            <button class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export as PDF</button>
        </div>
        <hr>
    </div>
        <div class="invoice overflow-auto">
            <div style="min-width: 600px">
                <header>
                    <div class="row">
                        <div class="col">
                            <a target="_blank" href="#">
                                <img src="http://localhost/sellerdashboard/assets/images/profile.jpeg" width="50%" data-holder-rendered="true" />
                                </a>
                        </div>
                        <div class="col company-details">
                            <h2 class="name">
                                <a target="_blank" href="https://lobianijs.com">
                                Chethan Raj
                                </a>
                            </h2>
                            <div>HSR Layout</div>
                            <div>9988776655</div>
                            <div>chethan@gmail.com</div>
                        </div>
                    </div>
                </header>
                <main>
                    <div class="row contacts">
                        <div class="col invoice-to">
                            <div class="text-gray-light">INVOICE TO:</div>
                            <h2 class="to">Leaf Furniture</h2>
                            <div class="address">HSR Layout, Bangalore</div>
                            <div class="email"><a href="chethankumar@vyavahar.co.in">info@leaffurniture.co.in</a></div>
                        </div>
                        <div class="col invoice-details">
                            <h1 class="invoice-id">INVOICE Number (212)</h1>
                            <div class="date">Date of Invoice: 01/10/2020</div>
                            <div class="date">Due Date: 30/10/2020</div>
                        </div>
                    <!-- </div> -->
                    <table border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th class="text-left">DESCRIPTION</th>
                                <th class="text-right">DISCOUNT</th>
                                <th class="text-right">QUANTITY</th>
                                <th class="text-right">TOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="no">04</td>
                                <td class="text-left"><h3>
                                    <a target="_blank" href="">
                                    Books
                                    </a>
                                    </h3> 
                                   Lorem Ipsum is simply dummy text of the printing and typesetting industry
                                </td>
                                <td class="unit">$0.00</td>
                                <td class="qty">100</td>
                                <td class="total">$0.00</td>
                            </tr>
                            <tr>
                                <td class="no">01</td>
                                <td class="text-left"><h3> Chair</h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry</td>
                                <td class="unit">$40.00</td>
                                <td class="qty">30</td>
                                <td class="total">$1,200.00</td>
                            </tr>
                            <tr>
                                <td class="no">02</td>
                                <td class="text-left"><h3>Mobile</h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry</td>
                                <td class="unit">$40.00</td>
                                <td class="qty">80</td>
                                <td class="total">$3,200.00</td>
                            </tr>
                            <tr>
                                <td class="no">03</td>
                                <td class="text-left"><h3>Search Engines Optimization</h3>Optimize the site for search engines (SEO)</td>
                                <td class="unit">$40.00</td>
                                <td class="qty">20</td>
                                <td class="total">$800.00</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">SUBTOTAL</td>
                                <td>$5,200.00</td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">TAX 25%</td>
                                <td>$1,300.00</td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">GRAND TOTAL</td>
                                <td>$6,500.00</td>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="thanks">Thank you!</div>
                    <div class="notices">
                        <div>NOTICE:</div>
                        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
                    </div>
                </main>
                <footer>
                    Invoice was created on a computer and is valid without the signature and seal.
                </footer>
            </div>
            <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
            <div></div>
        </div>
    </div>           
 </div>
        <!-- /.content -->

        <div class="clearfix"></div>

    </div>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    
    <!-- Search Data -->
   <!-- <script type="text/javascript">
       $("#sdate").change(function(){
        window.location.assign("<?php echo base_url().'Logs/Sort/'.$SField; ?>/" + $("#sdate").val());
    });

    $(function() {
        $( "#sdate" ).datepicker({
            minDate: -15,
            maxDate: "+0M +0D",
            dateFormat: 'yy-mm-dd'
        });
    });
    
    $(document).ready(function(){
      $("#SearchBox").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#LogData tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });-->

     $('#printInvoice').click(function(){
            Popup($('.invoice')[0].outerHTML);
            function Popup(data) 
            {
                window.print();
                return true;
            }
        });

    </script>
</body>
</html>
