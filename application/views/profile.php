<!doctype html>
<html class="no-js" lang="">
    <meta charset="utf-8">
    <title>Vyavahar - Profile</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Include Table Fixed Header CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">

</head>

<body>
    
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default" style="background-color: #ffffff;border-color: #ffffff;">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">

                    <li class="active">
                        <a style="color: black" href="<?php echo base_url('Dashboard');?>" ><i class="fa fa-home fa-fw fa-2x"></i>Dashboard</a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Orders');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Orders </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Packing Materials </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Returns </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Products');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Products </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Inventory');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Inventory </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Statement');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Statement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Invoice');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Invoice </a>
                    </li>

                    <li class="">
                        <a style="color: black" href=""><i class="fa fa-product-hunt fa-fw fa-2x"></i>Advertisement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Logs');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Logs </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Users');?>"><i class="fa fa-user-plus fa-fw fa-2x"></i>Users </a>
                    </li>                 
                </ul>
            </div>
        </nav>
    </aside>
    
    <div id="right-panel" class="right-panel">
        <!-- Header-->
         <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                     <a class="img-responsive navbar-brand" href="#"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo"></a> 

                    <a class="img-responsive navbar-brand hidden" href="./"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo" width="50%"></a>

                        <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>

                </div>
            </div>

            <div class="top-right">
                <div class="header-menu">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="img-responsive user-avatar rounded-circle" src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?php echo base_url('Profile');?>"><i class="fa fa- user"></i>My Profile</a>

                            <a class="nav-link" href="<?php echo base_url('Login/Logout');?>"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- Content -->
        <div class="content">
            <!--Div to show top error message-->
            <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>
            <!--Div to show top error message-->
            <div id="result"><?php if($this->session->flashdata('result')){echo $this->session->flashdata('result');} ?></div>
            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>

            <div class="row">
                <div class="col-sm-6">
                    <h4 style="font-weight: 600"><?php echo $title;?> </h4>
                </div>

                <div class="visible-sm"><br></div>

                <div class="col-sm-6" style="margin-top: 5px; text-align: right;">
                   
                </div> 
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body card-block">
                            <!--Profile Fieldset-->
                            <fieldset>
                                <legend><h1>Seller Profile</h1></legend>
                                <div class="row">
                                   <!-- Personal Details -->
                                   <div class="col-sm-6">
                                        <!--Profile Picture-->
                                        <div class="row">
                                            <div class="col-xs-3">
                                            <p><strong>Profile Picture: </strong></p>
                                            </div>
                                            <div class=" col-md-6">
                                                    <input type="file" id="Bimg" name="Bimg" value="" placeholder="Image " class="form-control">
                                                     <small><span class="error_txt" id="error_bimage"><?php if($this->session->flashdata('error_bimage')){echo $this->session->flashdata('error_bimage');} ?></span></small>
                                            </div>
                                        </div>                                        
                                        <!--Name-->
                                        <div class="row">
                                            <div class="col-xs-3">
                                            <p><strong>Name: </strong></p>
                                            </div>
                                            <div class="col-xs-9">
                                            <span><?php echo $this->session->userdata('fname'); ?>&nbsp;<?php echo $this->session->userdata('lname'); ?></span>
                                            </div>
                                        </div>
                                        <!--Email-->
                                        <div class="row">
                                            <div class="col-xs-3">
                                            <p><strong>Email: </strong></p>
                                            </div>
                                            <div class="col-xs-9">
                                            <span><?php echo $this->session->userdata('uid'); ?></span>
                                            </div>
                                        </div>
                                        <!--Mobile-->
                                        <div class="row">
                                            <div class="col-xs-3">
                                            <p><strong>Mobile: </strong></p>
                                            </div>
                                            <div class="col-xs-9">
                                            <span><?php echo $this->session->userdata('phone'); ?></span>
                                            </div>
                                        </div>
                                        <!--vyavahar id-->
                                        <div class="row">
                                            <div class="col-xs-3">
                                            <p><strong>Vyavahar ID: </strong></p>
                                            </div>
                                            <div class="col-xs-9">
                                            <span><?php echo $this->session->userdata(''); ?></span>
                                            </div>
                                        </div>
                                        <!--pan number-->
                                        <div class="row">
                                            <div class="col-xs-3">
                                            <p><strong>PAN: </strong></p>
                                            </div>
                                            <div class="col-xs-9">
                                            <span><?php echo $this->session->userdata(''); ?></span>
                                            </div>
                                        </div>
                                        <!--aadhaar-->
                                        <div class="row">
                                            <div class="col-xs-3">
                                            <p><strong>AADHAAR: </strong></p>
                                            </div>
                                            <div class="col-xs-9">
                                            <span><?php echo $this->session->userdata(''); ?></span>
                                            </div>
                                        </div>
                                        <!--Company name-->
                                        <div class="row">
                                            <div class="col-xs-3">
                                            <p><strong>Company Name: </strong></p>
                                            </div>
                                            <div class="col-xs-9">
                                            <span><?php echo $this->session->userdata(''); ?></span>
                                            </div>
                                        </div>
                                        <!--Role-->
                                        <div class="row">
                                            <div class="col-xs-3">
                                            <p><strong>Role:  </strong></p>
                                            </div>
                                            <div class="col-xs-9">
                                            <span><?php echo $this->session->userdata('role'); ?></span>
                                            </div>
                                        </div>
                                        <!--Status-->
                                        <div class="row">
                                            <div class="col-xs-3">
                                            <p><strong>Status:  </strong></p>
                                            </div>
                                            <div class="col-xs-9">
                                            <span><?php echo $this->session->userdata('status'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                <fieldset>
                                    <!-- Change Password -->
                                    <legend><h1>Change Password:</h1></legend>                                   
                                    <?php echo form_open('Profile/ChangeLoginPassword', array('class' => 'form-horizontal'))?>
                                        <!-- Current Password -->
                                        <div class="form-group">
                                            <div class="col-sm-5">
                                                <label>Current Password:</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="password" name="CurrentPassword" class="form-control" id="CurrentPassword" placeholder="Enter old password">
                                                <small><span class="error_txt" id="error_mode"><?php if($this->session->flashdata('error_currentpassword')){echo $this->session->flashdata('error_currentpassword');} ?></span></small>
                                            </div>
                                        </div>

                                        <!-- New Password -->
                                        <div class="form-group">
                                            <div class="col-sm-5">
                                                <label>New Password:</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="password" name="NewPassword" class="form-control" id="NewPassword" placeholder="Enter new password">
                                                <small><span class="error_txt" id="error_mode"><?php if($this->session->flashdata('error_newpassword')){echo $this->session->flashdata('error_newpassword');} ?></span></small>
                                            </div>
                                        </div>
                                        <!-- Confirm Password -->
                                        <div class="form-group">
                                            <div class="col-sm-5">
                                                <label>Confirm Password:</label>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="password" name="ConfirmPassword" class="form-control" id="ConfirmPassword" placeholder="Enter new password again">
                                                <small><span class="error_txt" id="error_mode"><?php if($this->session->flashdata('error_confirmpassword')){echo $this->session->flashdata('error_confirmpassword');} ?></span></small>
                                            </div>
                                        </div>
                                        <!-- Change Button -->
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-success pull-right">Change</button>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>
                                   </fieldset>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
        <!-- /.content -->
        <div class="clearfix"></div>
        <!-- Footer -->
        
    </div>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <script type="text/javascript">
            // Menu Trigger
        $('#menuToggle').on('click', function(event) {
            var windowWidth = $(window).width();         
            if (windowWidth<1010) { 
                $('body').removeClass('open'); 
                if (windowWidth<760){ 
                    $('#left-panel').slideToggle(); 
                } else {
                    $('#left-panel').toggleClass('open-menu');  
                } 
            } else {
                $('body').toggleClass('open');
                $('#left-panel').removeClass('open-menu');  
            } 
                 
        }); 

         
        $(".menu-item-has-children.dropdown").each(function() {
            $(this).on('click', function() {
                var $temp_text = $(this).children('.dropdown-toggle').html();
                $(this).children('.sub-menu').prepend('<li class="subtitle">' + $temp_text + '</li>'); 
            });
        });

            // Load Resize 
        $(window).on("load resize", function(event) { 
            var windowWidth = $(window).width();         
            if (windowWidth<1010) {
                $('body').addClass('small-device'); 
            } else {
                $('body').removeClass('small-device');  
            } 
            
        });

        $.noConflict();
    </script>
    <script>
    $(document).ready(function(){
        
        //Ajax Call for Change Password Form Submit
        $('#ChangePasswordForm').on('submit', function(form){
           form.preventDefault();
            $.ajax({  
                 url:"<?php echo base_url(); ?>Profile/ChangeLoginPassword",   
                 method:"POST",  
                 data:new FormData(this),  
                 contentType: false,  
                 cache: false,  
                 processData:false,  
                 success:function(data){  
                    //console.log(data);
                    $('#ErrorCurrentPassword').html(data['error_currentpassword']);
                    $('#ErrorNewPassword').html(data['error_newpassword']);
                    $('#ErrorConfirmPassword').html(data['error_confirmpassword']);
                    $('#ChangePasswordResult').html(data['result']);
                    
                    if(data['result_code']=='Success'){
                        $('#ChangePasswordForm').trigger('reset');
                    }
                 },
                 fail:function(data){
                }
            });
            return false;
        });
        
        $('#CloseChangePassword').on('click', function(){
            $('#ChangePasswordResult').text('');
            $('#ChangePasswordForm').trigger('reset');
        })
    });
</script>
</body>
</html>
