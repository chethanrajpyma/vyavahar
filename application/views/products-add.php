<!doctype html>
<html class="no-js" lang="">
    <meta charset="utf-8">
    <title>Vyavahar - Add Products</title>

    <link rel="icon" href="<?php echo base_url(); ?>assets/images/logo.jpeg">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Include Table Fixed Header CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">
</head>

<body>
    
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default" style="background-color: #ffffff;border-color: #ffffff;">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">

                   <li class="active">
                        <a style="color: black" href="<?php echo base_url('Dashboard');?>" ><i class="fa fa-home fa-fw fa-2x"></i>Dashboard</a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Orders');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Orders </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Packing Materials </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Returns </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Products');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Products </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Inventory');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Inventory </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Statement');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Statement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="#"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Invoice </a>
                    </li>

                    <li class="">
                        <a style="color: black" href=""><i class="fa fa-product-hunt fa-fw fa-2x"></i>Advertisement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Logs');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Logs </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Users');?>"><i class="fa fa-user-plus fa-fw fa-2x"></i>Users </a>
                    </li>   
                </ul>
            </div>
        </nav>
    </aside>
    
    <div id="right-panel" class="right-panel">
        <!-- Header-->
         <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                     <a class="img-responsive navbar-brand" href="#"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo"></a> 

                    <a class="img-responsive navbar-brand hidden" href="./"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo" width="50%"></a>

                        <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>

                </div>

            </div>

            <div class="top-right">
                <div class="header-menu">

                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="img-responsive user-avatar rounded-circle" src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?php echo base_url('Profile');?>"><i class="fa fa- user"></i>My Profile</a>

                            <a class="nav-link" href="<?php echo base_url('Login/Logout');?>"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- Content -->
        <div class="content">

            <!--Div to show top error message-->
            <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>
            <!--Div to show top error message-->
            <div id="result"><?php if($this->session->flashdata('result')){echo $this->session->flashdata('result');} ?></div>
            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>

            <div class="row">
                <div class="col-sm-12">
                    <h4 style="font-weight: 600"><?php echo $title;?></h4>
                </div>

                <div class="visible-sm"><br></div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body card-block">
                            <!--form open-->
                            <?php echo form_open_multipart(base_url('Products/SaveNewProducts')); ?>

                                <!-- First Category  Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>First Category  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pname" name="pname" value="<?php if($this->session->flashdata('fpname')){ echo $this->session->flashdata('fpname');}?>" placeholder="First Category" class="form-control">

                                         <small><span class="error_txt" id="error_pname"><?php if($this->session->flashdata('error_pname')){echo $this->session->flashdata('error_pname');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Second Category Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Second Category <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pname" name="pname" value="<?php if($this->session->flashdata('fpname')){ echo $this->session->flashdata('fpname');}?>" placeholder="Second  Category" class="form-control">

                                         <small><span class="error_txt" id="error_pname"><?php if($this->session->flashdata('error_pname')){echo $this->session->flashdata('error_pname');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Third Category Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Third Category <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pname" name="pname" value="<?php if($this->session->flashdata('fpname')){ echo $this->session->flashdata('fpname');}?>" placeholder="Third Category" class="form-control">

                                         <small><span class="error_txt" id="error_pname"><?php if($this->session->flashdata('error_pname')){echo $this->session->flashdata('error_pname');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Brand Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Brand <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pname" name="pname" value="<?php if($this->session->flashdata('fpname')){ echo $this->session->flashdata('fpname');}?>" placeholder="Brand" class="form-control">

                                         <small><span class="error_txt" id="error_pname"><?php if($this->session->flashdata('error_pname')){echo $this->session->flashdata('error_pname');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Colors Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Colors <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pname" name="pname" value="<?php if($this->session->flashdata('fpname')){ echo $this->session->flashdata('fpname');}?>" placeholder="Color" class="form-control">

                                         <small><span class="error_txt" id="error_pname"><?php if($this->session->flashdata('error_pname')){echo $this->session->flashdata('error_pname');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Product Name Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Product Name <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pname" name="pname" value="<?php if($this->session->flashdata('fpname')){ echo $this->session->flashdata('fpname');}?>" placeholder="Products Name" class="form-control">

                                         <small><span class="error_txt" id="error_pname"><?php if($this->session->flashdata('error_pname')){echo $this->session->flashdata('error_pname');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Product Full Name Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Product Full Name <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pname" name="pname" value="<?php if($this->session->flashdata('fpname')){ echo $this->session->flashdata('fpname');}?>" placeholder="Product Full Name" class="form-control">

                                         <small><span class="error_txt" id="error_pname"><?php if($this->session->flashdata('error_pname')){echo $this->session->flashdata('error_pname');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Product Title Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Products Category <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pcateg" name="pcateg" value="<?php if($this->session->flashdata('fpcateg')){ echo $this->session->flashdata('fpcateg');}?>" placeholder="Products Category" class="form-control">

                                         <small><span class="error_txt" id="error_pcateg"><?php if($this->session->flashdata('error_pcateg')){echo $this->session->flashdata('error_pcateg');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Product Description Name Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Product Description  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <textarea type="text" name="descp" id="editor1" value="<?php if($this->session->flashdata('fpdesc')){ echo $this->session->flashdata('fpdesc');}?>" placeholder="Descriptions...." class="form-control"><?php if($this->session->flashdata('fpdesc')){ echo $this->session->flashdata('fpdesc');}?></textarea>
                                        <small><span class="error_txt" id="error_fdescp"><?php if($this->session->flashdata('error_fdescp')){echo $this->session->flashdata('error_fdescp');} ?></span></small>
                                    </div>
                                </div>

                                <!--  Printed Price -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Printed Price <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pprice" name="pprice" value="<?php if($this->session->flashdata('fpprice')){ echo $this->session->flashdata('fpprice');}?>" placeholder=" Printed Price" class="form-control">

                                         <small><span class="error_txt" id="error_pprice"><?php if($this->session->flashdata('error_pprice')){echo $this->session->flashdata('error_pprice');} ?></span></small>
                                    </div>
                                </div>

                                <!--  Selling Price * -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Selling Price <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pprice" name="pprice" value="<?php if($this->session->flashdata('fpprice')){ echo $this->session->flashdata('fpprice');}?>" placeholder=" Selling Price " class="form-control">

                                         <small><span class="error_txt" id="error_pprice"><?php if($this->session->flashdata('error_pprice')){echo $this->session->flashdata('error_pprice');} ?></span></small>
                                    </div>
                                </div>
                                
                                <!--  Disount Price Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Discount Price <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="discount" name="discount" value="<?php if($this->session->flashdata('fdiscount')){ echo $this->session->flashdata('fdiscount');}?>" placeholder=" Discount Price" class="form-control">

                                         <small><span class="error_txt" id="error_discount"><?php if($this->session->flashdata('error_discount')){echo $this->session->flashdata('error_discount');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Final Price Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Final Price <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pprice" name="pprice" value="<?php if($this->session->flashdata('fpprice')){ echo $this->session->flashdata('fpprice');}?>" placeholder="Final Price" class="form-control">

                                         <small><span class="error_txt" id="error_pprice"><?php if($this->session->flashdata('error_pprice')){echo $this->session->flashdata('error_pprice');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Stocks Available  Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Stocks Available <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pprice" name="pprice" value="<?php if($this->session->flashdata('fpprice')){ echo $this->session->flashdata('fpprice');}?>" placeholder="Stock Available " class="form-control">

                                         <small><span class="error_txt" id="error_pprice"><?php if($this->session->flashdata('error_pprice')){echo $this->session->flashdata('error_pprice');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Product Maximum Order Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Product Maximum Order <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pprice" name="pprice" value="<?php if($this->session->flashdata('fpprice')){ echo $this->session->flashdata('fpprice');}?>" placeholder=" Product Maximum Order " class="form-control">

                                         <small><span class="error_txt" id="error_pprice"><?php if($this->session->flashdata('error_pprice')){echo $this->session->flashdata('error_pprice');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Product Minimum Order Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Product Minimum Order <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pprice" name="pprice" value="<?php if($this->session->flashdata('fpprice')){ echo $this->session->flashdata('fpprice');}?>" placeholder=" Product Minimum Order " class="form-control">

                                         <small><span class="error_txt" id="error_pprice"><?php if($this->session->flashdata('error_pprice')){echo $this->session->flashdata('error_pprice');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Product Code Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Product Code <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pprice" name="pprice" value="<?php if($this->session->flashdata('fpprice')){ echo $this->session->flashdata('fpprice');}?>" placeholder=" Product Code " class="form-control">

                                         <small><span class="error_txt" id="error_pprice"><?php if($this->session->flashdata('error_pprice')){echo $this->session->flashdata('error_pprice');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Product Availiblity Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Product Availiblity <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pprice" name="pprice" value="<?php if($this->session->flashdata('fpprice')){ echo $this->session->flashdata('fpprice');}?>" placeholder=" Product Availiblity " class="form-control">

                                         <small><span class="error_txt" id="error_pprice"><?php if($this->session->flashdata('error_pprice')){echo $this->session->flashdata('error_pprice');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Product Video URL Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Product Video URL <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pprice" name="pprice" value="<?php if($this->session->flashdata('fpprice')){ echo $this->session->flashdata('fpprice');}?>" placeholder=" Product Video URL " class="form-control">

                                         <small><span class="error_txt" id="error_pprice"><?php if($this->session->flashdata('error_pprice')){echo $this->session->flashdata('error_pprice');} ?></span></small>
                                    </div>
                                </div>

                            
                                <!-- Product Quantaty Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Products Quantaty <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="quantaty" name="quantaty" value="<?php if($this->session->flashdata('fquantaty')){ echo $this->session->flashdata('fquantaty');}?>" placeholder="Products Quantaty" class="form-control">

                                         <small><span class="error_txt" id="error_quantaty"><?php if($this->session->flashdata('error_quantaty')){echo $this->session->flashdata('error_quantaty');} ?></span></small>
                                    </div>
                                </div>

                                <!--  Product Price Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Products Size <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="psize" name="psize" value="<?php if($this->session->flashdata('fpsize')){ echo $this->session->flashdata('fpsize');}?>" placeholder=" Products Price" class="form-control">

                                         <small><span class="error_txt" id="error_psize"><?php if($this->session->flashdata('error_psize')){echo $this->session->flashdata('error_psize');} ?></span></small>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body card-block">

                                <!-- Banner Image  Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Banner Image  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="file" id="Bimg" name="Bimg" value="" placeholder="Image " class="form-control">
                                         <small><span class="error_txt" id="error_bimage"><?php if($this->session->flashdata('error_bimage')){echo $this->session->flashdata('error_bimage');} ?></span></small>
                                    </div>
                                </div>
                            
                                <!-- Thumb Image  Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Thumb Image  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="file" id="Timg" name="Timg" value="" placeholder="Image " class="form-control">
                                         <small><span class="error_txt" id="error_ttimage"><?php if($this->session->flashdata('error_ttimage')){echo $this->session->flashdata('error_ttimage');} ?></span></small>
                                         <small><p><i>Supported: PNG / JPG<br>Size : Max 2mb<br>image dimension should be 255 * 250 pixels</i></p></small>
                                    </div>
                                </div>

                                <!-- Main Image  Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Main Image  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="file" id="Mimg" name="Mimg" value="" placeholder="Image " class="form-control">
                                         <small><span class="error_txt" id="error_tmimage"><?php if($this->session->flashdata('error_tmimage')){echo $this->session->flashdata('error_tmimage');} ?></span></small>
                                         <small><p><i>Supported: PNG / JPG<br>Size : Max 2mb<br>image dimension should be 350 * 400 pixels</i></p></small>
                                    </div>
                                </div>

                                <!--   SGST Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> SGST <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="psize" name="psize" value="<?php if($this->session->flashdata('fpsize')){ echo $this->session->flashdata('fpsize');}?>" placeholder=" SGST " class="form-control">

                                         <small><span class="error_txt" id="error_psize"><?php if($this->session->flashdata('error_psize')){echo $this->session->flashdata('error_psize');} ?></span></small>
                                    </div>
                                </div>

                                <!--   CGST  Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> CGST <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="psize" name="psize" value="<?php if($this->session->flashdata('fpsize')){ echo $this->session->flashdata('fpsize');}?>" placeholder=" CGST " class="form-control">

                                         <small><span class="error_txt" id="error_psize"><?php if($this->session->flashdata('error_psize')){echo $this->session->flashdata('error_psize');} ?></span></small>
                                    </div>
                                </div>

                                <!--   IGST Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>  IGST <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="psize" name="psize" value="<?php if($this->session->flashdata('fpsize')){ echo $this->session->flashdata('fpsize');}?>" placeholder=" IGST " class="form-control">

                                         <small><span class="error_txt" id="error_psize"><?php if($this->session->flashdata('error_psize')){echo $this->session->flashdata('error_psize');} ?></span></small>
                                    </div>
                                </div>

                                 <!-- SEO Image  Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>SEO Image  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="file" id="Bimg" name="Bimg" value="" placeholder="Image " class="form-control">
                                         <small><span class="error_txt" id="error_bimage"><?php if($this->session->flashdata('error_bimage')){echo $this->session->flashdata('error_bimage');} ?></span></small>
                                    </div>
                                </div>

                                <!--  SEO Titles Name Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> SEO Titles  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <textarea type="text" name="descp" id="editor3" value="<?php if($this->session->flashdata('fpdesc')){ echo $this->session->flashdata('fpdesc');}?>" placeholder="SEO Titles...." class="form-control"><?php if($this->session->flashdata('fpdesc')){ echo $this->session->flashdata('fpdesc');}?></textarea>
                                        <small><span class="error_txt" id="error_fdescp"><?php if($this->session->flashdata('error_fdescp')){echo $this->session->flashdata('error_fdescp');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Product Tags Name Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Product Tags  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <textarea type="text" name="descp" id="editor2" value="<?php if($this->session->flashdata('fpdesc')){ echo $this->session->flashdata('fpdesc');}?>" placeholder="Products Tags...." class="form-control"><?php if($this->session->flashdata('fpdesc')){ echo $this->session->flashdata('fpdesc');}?></textarea>
                                        <small><span class="error_txt" id="error_fdescp"><?php if($this->session->flashdata('error_fdescp')){echo $this->session->flashdata('error_fdescp');} ?></span></small>
                                    </div>
                                </div>

                                <!--  SEO Description Name Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> SEO Description  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <textarea type="text" name="descp" id="editor4" value="<?php if($this->session->flashdata('fpdesc')){ echo $this->session->flashdata('fpdesc');}?>" placeholder="SEO Description...." class="form-control"><?php if($this->session->flashdata('fpdesc')){ echo $this->session->flashdata('fpdesc');}?></textarea>
                                        <small><span class="error_txt" id="error_fdescp"><?php if($this->session->flashdata('error_fdescp')){echo $this->session->flashdata('error_fdescp');} ?></span></small>
                                    </div>
                                </div> 

                                <!--  Product Price Field -->
                                <!-- <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Products Size <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="psize" name="psize" value="<?php if($this->session->flashdata('fpsize')){ echo $this->session->flashdata('fpsize');}?>" placeholder=" Products Price" class="form-control">

                                         <small><span class="error_txt" id="error_psize"><?php if($this->session->flashdata('error_psize')){echo $this->session->flashdata('error_psize');} ?></span></small>
                                    </div>
                                </div> -->

                                <!--  Product Price Field -->
                                <!-- <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Products Size <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="psize" name="psize" value="<?php if($this->session->flashdata('fpsize')){ echo $this->session->flashdata('fpsize');}?>" placeholder=" Products Price" class="form-control">

                                         <small><span class="error_txt" id="error_psize"><?php if($this->session->flashdata('error_psize')){echo $this->session->flashdata('error_psize');} ?></span></small>
                                    </div>
                                </div> -->

                                <!-- Save & Cancel Button -->
                                <div class="row">
                                    <div class="col-sm-12">

                                        <!-- Cancel Button -->
                                        <a href="<?php echo base_url('Products/')?>"><button type="button" id="canceluser" class="btn btn-default pull-right btn-sm"><span class="fa fa-times"></span> Cancel</button></a>
                                        <span class="pull-right">&nbsp;</span>

                                        <!-- Save Button -->
                                        <button type="submit" class="btn btn-success btn-sm pull-right"><span class="fa fa-save"></span> Save</button><br><br>
                                    </div>
                                </div>

                            <?php echo form_close() ?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Include Table Fixed Header CSS -->
            <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">   
        </div>
        <!-- /.content -->

        <div class="clearfix"></div>

    </div>

    <!-- CKEditer -->
    <script type="text/javascript" src="//cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
        window.onload=function(){
    
            // instance, using default configuration.
            CKEDITOR.editorConfig = function (config) {
                config.language = 'es';
                config.uiColor = '#F7B42C';
                config.height = 200;
                config.toolbarCanCollapse = true;

            };
            CKEDITOR.replace('editor1');

            // instance, using default configuration.
            CKEDITOR.editorConfig = function (config) {
                config.language = 'es';
                config.uiColor = '#F7B42C';
                config.height = 200;
                config.toolbarCanCollapse = true;

            };
            CKEDITOR.replace('editor2');

            // instance, using default configuration.
            CKEDITOR.editorConfig = function (config) {
                config.language = 'es';
                config.uiColor = '#F7B42C';
                config.height = 200;
                config.toolbarCanCollapse = true;

            };
            CKEDITOR.replace('editor3');

            // instance, using default configuration.
            CKEDITOR.editorConfig = function (config) {
                config.language = 'es';
                config.uiColor = '#F7B42C';
                config.height = 200;
                config.toolbarCanCollapse = true;

            };
            CKEDITOR.replace('editor4');

        }
    </script>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <!-- <script src="</?php echo base_url();?>assets/js/common.js"></script> -->
</body>
</html>
