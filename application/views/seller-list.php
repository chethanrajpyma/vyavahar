<!doctype html>
<html class="no-js" lang="">
    <meta charset="utf-8">

    <title>Vyavahar - Seller List</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Include Table Fixed Header CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">

    <style type="text/css">
        .SearchBox {
            width: 100%;
            height: 40px;
            padding: 0px 10px;
            margin-bottom: 10px;
            border: none;
            font-family: 'Open Sans', sans-serif;
        }
    </style>
</head>

<body>
    
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default" style="background-color: #ffffff;border-color: #ffffff;">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">

                   <li class="active">
                        <a style="color: black" href="<?php echo base_url('Dashboard');?>" ><i class="fa fa-home fa-fw fa-2x"></i>Dashboard</a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Orders');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Orders </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Packing Materials </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Returns </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Products');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Products </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Inventory');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Inventory </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Statement');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Statement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="#"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Invoice </a>
                    </li>

                    <li class="">
                        <a style="color: black" href=""><i class="fa fa-product-hunt fa-fw fa-2x"></i>Advertisement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Logs');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Logs </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Users');?>"><i class="fa fa-user-plus fa-fw fa-2x"></i>Users </a>
                    </li>   
                   <!--  <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cog"></i>Settings</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-chevron-right"></i><a href="<?php echo base_url('Settings');?>">Settings</a></li>
                            <li><i class="fa fa-chevron-right"></i><a href="<?php echo base_url('Books/TechnicalSupport');?>">Team</a></li>
                        </ul>
                    </li> -->
                </ul>
            </div>
        </nav>
    </aside>

    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="img-responsive navbar-brand" href="./"><img src="<?php echo base_url(); ?>assets/images/profile.jpeg" alt="Logo"></a>
                    <a class="img-responsive navbar-brand hidden" href="./"><img src="<?php echo base_url(); ?>assets/images/profile.jpeg" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">

                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="img-responsive user-avatar rounded-circle" src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?php echo base_url('Profile');?>"><i class="fa fa- user"></i>My Profile</a>

                            <a class="nav-link" href="<?php echo base_url('Login/Logout');?>"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <!-- Content -->
        <div class="content">
            <!--Div to show top error message-->
            <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>

            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>

            <div class="row">
                 <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <h4 style="font-weight: 600"><?php echo $title;?></h4>
                </div>

                <div class="visible-sm"><br></div>

                <div class="col-sm-5">
                    <div class="row form-group">
                        <label class="col-sm-4 control-label" style="margin-top: 5px; text-align: right;">Select State</label>
                        <div class="col-sm-8">
                            <select name="StateBox" id="StateBox" class="form-control">
                                <?php foreach($State->result() as $row){?>
                                <option value="<?php echo $row->sid;?>"<?php if($this->session->flashdata('flang')==$row->sid){ echo 'selected'; }?>><?php echo $row->sname;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="visible-sm"><br></div>

                <div class="col-sm-4">
                    <input id="SearchBox" type="text" name="search" class="SearchBox " placeholder="Search..">
                </div>
                
            </div>

            <section>
                <div style="width:100%; height:55vh; border-top:3px SOLID LIGHTGREY; overflow-y:scroll; overflow-x:scroll;">
                    <table id="CommonTable" class="TblFont">
                        <thead class="hidden-xs hidden-sm">
                            <tr>
                                <th class="nofocus" style="cursor:pointer;">ID<div> ID <i class="fa fa-sort"></i></div>
                                </th>
                                <th class="nofocus" style="cursor:pointer;">Seller<div> Seller <i class="fa fa-sort"></i></div>
                                </th>
                                <!-- <th class="nofocus" style="cursor:pointer;">Year<div>Year <i class="fa fa-sort"></i></div> -->
                                </th>
                                <th class="nofocus" style="cursor:pointer;">Name<div>Name <i class="fa fa-sort"></i></div>
                                </th>
                                <th class="nofocus" style="cursor:pointer;">Category<div>Category <i class="fa fa-sort"></i></div>
                                </th>
                                <th class="nofocus" style="cursor:pointer;">City<div>City <i class="fa fa-sort"></i></div>
                                </th>
                                <th class="nofocus" style="cursor:pointer;">District<div>District <i class="fa fa-sort"></i></div>
                                </th>
                                <th class="nofocus" style="cursor:pointer;">More<div>Status <i class="fa fa-sort"></i></div>
                                </th>
                                <th class="nofocus" style="cursor:pointer;">More<div>More <i class="fa fa-sort"></i></div>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="SearchData"></tbody>
                    </table>
                </div>
            </section>
            <br>    
            <!-- Display number of users -->
            <div class="row">
                <div class="col-sm-4">
                    <span><b>No. of Records :</b> <span id='DataCount'></span></span>
                </div>
                <div class="col-sm-8"></div>
            </div>           
        </div>
        <!-- /.content -->

        <div class="clearfix"></div>

        <!-- Footer -->
        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <!-- <div class="col-sm-12">
                        <p style="text-align: center;"></p>
                        <p style="text-align: center;">Designed by <a href="" target="_blank"><b style="color: blue">Vyavahar</b></a></p>
                    </div> -->
                </div>
            </div>
        </footer>
    </div>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    
    <script> var base_url='<?php echo base_url(); ?>'; </script>

    <!-- Search Data -->
    <script type="text/javascript">

        $(document).ready(function(){
            $('#SearchData').empty();
            var selectedValue = $("#StateBox").val();
             $.ajax({  
                 url:base_url+"Pyramids/GetStateWiseCenters/"+selectedValue,   
                 method:"GET",  
                 //data:new FormData(formvalue),  
                 contentType: false,  
                 cache: false,  
                 processData:false,  
                 success:function(data){
                    
                    if(data=='false'){
                        $('#SearchData').empty();
                    }else{
                        $('#CommonTable').append(data['rows']);
                        $('#DataCount').text(data['rowcount']);
                    }
                }
            });
        });

        $( "#StateBox" ).change(function() {
          var selectedValue = $("#StateBox").val();
            $('#SearchData').empty();
             $.ajax({  
                 url:base_url+"Pyramids/GetStateWiseCenters/"+selectedValue,   
                 method:"GET",  
                 //data:new FormData(formvalue),  
                 contentType: false,  
                 cache: false,  
                 processData:false,  
                 success:function(data){
                    
                    if(data=='false'){
                        $('#SearchData').empty();
                    }else{
                        $('#CommonTable').append(data['rows']);
                        $('#DataCount').text(data['rowcount']);
                    }
                }
            });
        });

        $(document).ready(function(){
            $("#SearchBox").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#SearchData tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });

        
        $(document).on('click', 'th', function() {
            var table = $(this).parents('table').eq(0);
            var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()));
            this.asc = !this.asc;
            if (!this.asc) {
                rows = rows.reverse();
            }
            table.children('tbody').empty().html(rows);
        });

        function comparer(index) {
            return function(a, b) {
                var valA = getCellValue(a, index),
                valB = getCellValue(b, index);
                return $.isNumeric(valA) && $.isNumeric(valB) ?
                valA - valB : valA.localeCompare(valB);
            };
        }

        function getCellValue(row, index) {
            return $(row).children('td').eq(index).text();
        }

        // Menu Trigger
        $('#menuToggle').on('click', function(event) {
            var windowWidth = $(window).width();         
            if (windowWidth<1010) { 
                $('body').removeClass('open'); 
                if (windowWidth<760){ 
                    $('#left-panel').slideToggle(); 
                } else {
                    $('#left-panel').toggleClass('open-menu');  
                } 
            } else {
                $('body').toggleClass('open');
                $('#left-panel').removeClass('open-menu');  
            } 
                 
        }); 

         
        $(".menu-item-has-children.dropdown").each(function() {
            $(this).on('click', function() {
                var $temp_text = $(this).children('.dropdown-toggle').html();
                $(this).children('.sub-menu').prepend('<li class="subtitle">' + $temp_text + '</li>'); 
            });
        });

            // Load Resize 
        $(window).on("load resize", function(event) { 
            var windowWidth = $(window).width();         
            if (windowWidth<1010) {
                $('body').addClass('small-device'); 
            } else {
                $('body').removeClass('small-device');  
            } 
            
        });

        $.noConflict();

    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
</body>
</html>
