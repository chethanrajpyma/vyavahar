<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    if($this->session->flashdata('flashuid')!=''){
        $flashuid = $this->session->flashdata('flashuid');  
    }else{
        $flashuid = '';
    }

    if($this->session->flashdata('flashpwd')!=''){
        $flashpwd = $this->session->flashdata('flashpwd');  
    }else{
        $flashpwd = '';
    }
?>

<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login | Vyavahar - Admin</title>

    <link rel="icon" href="<?php echo base_url(); ?>assets/images/logo.jpeg">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <style type="text/css">
        a:hover, a:focus {
            color: #0056b3 !important;
            text-decoration: none;
            font-weight: 700;
        }
    </style>  
</head>
<body style="background-color: #4150ff17!important;">

    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-form">
                    <!--Login form-->
                    <?php echo form_open(base_url().'Login/Login'); ?>
                        <div class="login-logo">
                            <a href="#">
                                <img class="align-content" src="<?php base_url();?>assets/images/logo.jpeg" alt="Logo" width="50%">
                            </a>
                        </div><br>

                        <!-- Errorr message -->
                        <div id="errors" class="login_errors">
                           <!--  <?php
                                if($this->session->flashdata('errors')){
                                    echo $this->session->flashdata('errors');
                                }else{
                                    echo '<div class="alert alert-success LoginAlert"><span id="LoginMsg">To enter <strong>PEW-Admin</strong> please sign in using<br> e-mail and password</span></div>';
                                }
                            ?> -->
                        </div>
                        <br>

                        <!--User ID-->
                        <div class="row input-group">
                            <div class="col-sm-3">
                                <label>Username</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="userid" id="userid" class="form-control" placeholder="Email" value="<?php echo $flashuid; ?>">
                                <span class="login_errors">
                                    <?php
                                        if($this->session->flashdata('error_userid')){
                                            echo $this->session->flashdata('error_userid');
                                        }
                                    ?>
                                </span>
                            </div>
                        </div>
                        <br>

                        <!--Password-->
                        <div class="row input-group">
                            <div class="col-sm-3">
                                <label>Password</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="password" name="password" id="pwd" class="form-control" value="<?php echo $flashpwd; ?>" placeholder="Password">
                                <span class="login_errors">
                                    <?php
                                        if($this->session->flashdata('error_password')){
                                            echo $this->session->flashdata('error_password');
                                        }
                                    ?>
                                </span>
                            </div>
                        </div>
                        <br>

                        <!-- Forgot Password-->
                        <div class="row input-group">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-3">
                                <input type="submit" class="btn btn-success btn-sm center-block" value="Sign in">
                            </div>
                            <!-- <div class="col-sm-6">
                                <a href="</?php echo base_url('ResetPassword'); ?>" style="float:right;font-size: 18px;">Forgot Password?</a>
                            </div> -->

                        </div>

                    <?php echo form_close() ?>
                    <!--Login form close-->
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>

</body>
</html>
