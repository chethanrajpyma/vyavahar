 <?php include APPPATH.'views/include/header.php';?>

        <!-- start page title -->
        <div class="row">
          <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
              <h4 class="mb-0 font-size-18">Add New Buyer</h4>
             
            </div>
          </div>
        </div>
        <!-- end page title -->
        
        <div class="row">
          <div class="col-xl-12">
            <div class="card" style="    margin-top: 15px;">
              <div class="card-body" style="border:1px solid #ddd">
              <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>
            <!--Div to show top error message-->
            <div id="result"><?php if($this->session->flashdata('result')){echo $this->session->flashdata('result');} ?></div>
            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>
			
             <form method="post" action="<?php base_url('Buyer/SaveNewBuyer')?>">  
                  <div class="form-group">
                    <label for="username">First Name</label>
                    <input type="text" id="fname" name="fname" class="form-control" placeholder="Enter First Name"  required>
                  </div>
                  <div class="form-group">
                    <label for="phone">Last Name</label>
                     <input type="text" id="fname" name="fname" class="form-control" placeholder="Enter Last Name"  required>
                  </div>
                  
                  <br>
				  
				  <div class="row">
					<div class="col-sm-12">

						<!-- Cancel Button -->
						<a href="<?php echo base_url('Buyer/')?>"><button type="button" id="canceluser" class="btn btn-default pull-right btn-sm"><span class="fa fa-times"></span> Cancel</button></a>
						<span class="pull-right">&nbsp;</span>

						<!-- Save Button -->
						<button type="submit" class="btn btn-success btn-sm pull-right"><span class="fa fa-save"></span> Save</button><br><br>
					</div>
				</div>
								
				
                </form>
              </div>
              <!-- end card-body--> 
            </div>
          </div>
        </div>
      </div>
      <!-- container-fluid --> 
    </div>
    <!-- End Page-content -->
    
  
  </div>
  
  
  
    
    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <!-- <script src="</?php echo base_url();?>assets/js/common.js"></script> -->
</body>
</html>
