<!doctype html>
<html class="no-js" lang="">
    <meta charset="utf-8">
    <title>Vyavahar - Dashboard</title>

    <link rel="icon" href="<?php echo base_url(); ?>assets/images/logo.jpeg">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Include Table Fixed Header CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">

</head>

<body>
    
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default" style="background-color: #ffffff;border-color: #ffffff;">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">

                    <li class="active">
                        <a style="color: black" href="<?php echo base_url('Dashboard');?>" ><i class="fa fa-home fa-fw fa-2x"></i>Dashboard</a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Orders');?>"><i class="fa fa-bullhorn fa-fw fa-2x"></i>Orders </a>
                    </li>

                   <!--  <li class="menu-item-has-children dropdown">
                        <a  class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-product-hunt fa-fw fa-2x" style="font-size: 30px;margin-top: -3px;margin-left: -2px;"></i>Sellers</a>

                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-chevron-right"></i><a href="<?php echo base_url('Seller/SellerMaster');?>">View Sellers</a></li>
                            <li><i class="fa fa-chevron-right"></i><a href="<?php echo base_url('Seller/GetSellerMaster');?>">Approved Sellers</a></li>
                            <li><i class="fa fa-chevron-right"></i><a href="<?php echo base_url('Sellers/AddProducts');?>">Rejected Sellers</a></li>
                        </ul>

                    </li> -->

                     <!-- <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-balance-scale fa-fw fa-2x" style="font-size: 30px;margin-top: -3px;margin-left: -2px;"></i>Buyers</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-chevron-right"></i><a href="<?php echo base_url('Buyers/AddProducts');?>">All Buyers</a></li>
                            <li><i class="fa fa-chevron-right"></i><a href="<?php echo base_url('Buyers/AddProducts');?>">Approved Buyers</a></li>
                            <li><i class="fa fa-chevron-right"></i><a href="<?php echo base_url('Buyers/AddProducts');?>">Rejected Buyers</a></li>
                        </ul>
                    </li>  -->


                  <!--   <li class="">
                        <a style="color: black" href="<?php echo base_url('Orders');?>"><i class="fa fa-balance-scale fa-fw fa-2x"></i>Buyers </a>
                  </li> -->
 
                <!-- <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Packing Materials </a>
                    </li> --> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Returns </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Products');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Cataloge </a>
                    </li>

                       <!-- ---------------------------------------------- -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-balance-scale" style="font-size: 30px;margin-top: -3px;margin-left: -2px;"></i>Sellers</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-chevron-right"></i><a href="<?php echo base_url('Sellers');?>">All Sellers</a></li>
                            <li><i class="fa fa-chevron-right"></i><a href="<?php echo base_url('Sellers/SpecialPyramids');?>">Approved Seller</a></li>
                            <li><i class="fa fa-chevron-right"></i><a href="<?php echo base_url('Sellers/AncientPyramids');?>">Approved Sellers</a></li>
                        </ul>
                    </li>
                    <!-- ---------------------------------------------- -->

                    <!-- <li class="">
                        <a style="color: black" href="<?php echo base_url('Inventory');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Inventory </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Statement');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Statement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Invoice');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Invoice </a>
                    </li>

                    <li class="">
                        <a style="color: black" href=""><i class="fa fa-product-hunt fa-fw fa-2x"></i>Advertisement </a>
                    </li>  -->

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Logs');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Logs </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Users');?>"><i class="fa fa-user-plus fa-fw fa-2x"></i>Users </a>
                    </li>
                </ul>
            </div>
        </nav>
    </aside>
    
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                     <a class="img-responsive navbar-brand" href="#"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo"></a> 

                    <a class="img-responsive navbar-brand hidden" href="./"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo" width="50%"></a>

                        <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>

                </div>

            </div>

            <div class="top-right">
                <div class="header-menu">

                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="img-responsive user-avatar rounded-circle" src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo">
                        </a>


                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?php echo base_url('Profile');?>"><i class="fa fa- user"></i>My Profile</a>

                            <a class="nav-link" href="<?php echo base_url('Login/Logout');?>"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <!-- Content -->
        <div class="content">
            <!--Div to show top error message-->
            <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>

            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>

            <div class="row">

                <div class="col-sm-12">
                    <h4 style="font-weight: 600"><!-- </?php echo $title;?> --></h4>
                </div>

                <div class="visible-sm"><br></div> 
            </div>

            <div class="row">
                <div class="col-sm-2">
                    <b>Total Sellers</b>
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <b>Total Buyers</b>
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <b>  Total Orders</b>
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <b> Total Returns</b>
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <b> Present Orders</b>
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <b> Present Delivery</b>
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body card-block" style="padding: 168px;">
                            <p style="font-size: 20px;font-weight: 700;color: black;text-align: center;">Welcome to Vyavahar Dashboard</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </div>
   </div>
        <!-- /.content -->
        <div class="clearfix"></div>

    </div>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <script type="text/javascript">
        // Menu Trigger
        $('#menuToggle').on('click', function(event) {
            var windowWidth = $(window).width();         
            if (windowWidth<1010) { 
                $('body').removeClass('open'); 
                if (windowWidth<760){ 
                    $('#left-panel').slideToggle(); 
                } else {
                    $('#left-panel').toggleClass('open-menu');  
                } 
            } else {
                $('body').toggleClass('open');
                $('#left-panel').removeClass('open-menu');  
            } 
                 
        }); 

         
        $(".menu-item-has-children.dropdown").each(function() {
            $(this).on('click', function() {
                var $temp_text = $(this).children('.dropdown-toggle').html();
                $(this).children('.sub-menu').prepend('<li class="subtitle">' + $temp_text + '</li>'); 
            });
        });

            // Load Resize 
        $(window).on("load resize", function(event) { 
            var windowWidth = $(window).width();         
            if (windowWidth<1010) {
                $('body').addClass('small-device'); 
            } else {
                $('body').removeClass('small-device');  
            } 
            
        });

        $.noConflict();
    </script>
</body>
</html>
d