<!doctype html>
<html class="no-js" lang="">
    <meta charset="utf-8">
    <title> Vyavahar - Edit Orders</title>

    <link rel="icon" href="<?php echo base_url(); ?>assets/images/logo.jpeg">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Include Table Fixed Header CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">
</head>

<body>
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default" style="background-color: #ffffff;border-color: #ffffff;">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">

                   <li class="active">
                        <a style="color: black" href="<?php echo base_url('Dashboard');?>" ><i class="fa fa-home fa-fw fa-2x"></i>Dashboard</a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Orders');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Orders </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Packing Materials </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Returns </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Products');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Products </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Inventory');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Inventory </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Statement');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Statement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="#"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Invoice </a>
                    </li>

                    <li class="">
                        <a style="color: black" href=""><i class="fa fa-product-hunt fa-fw fa-2x"></i>Advertisement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Logs');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Logs </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Users');?>"><i class="fa fa-user-plus fa-fw fa-2x"></i>Users </a>
                    </li>   
                </ul>
            </div>
        </nav>
    </aside>
    
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                     <a class="img-responsive navbar-brand" href="#"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo"></a> 

                     <a class="img-responsive navbar-brand hidden" href="./"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo" width="50%"></a>
              
                     <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>

            <div class="top-right">
                <div class="header-menu">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="img-responsive user-avatar rounded-circle" src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo">
                        </a>
                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?php echo base_url('Profile');?>"><i class="fa fa- user"></i>My Profile</a>
                            <a class="nav-link" href="<?php echo base_url('Login/Logout');?>"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- Content -->
        <div class="content">

            <!--Div to show top error message-->
            <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>
            <!--Div to show top error message-->
            <div id="result"><?php if($this->session->flashdata('result')){echo $this->session->flashdata('result');} ?></div>
            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>

            <div class="row">
                <div class="col-sm-12">
                    <h4 style="font-weight: 600"><?php echo $title;?></h4>
                </div>

                <div class="visible-sm"><br></div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body card-block">
                            <!--form open-->
                            <?php echo form_open_multipart(base_url('Orders/UpdateOrders')); ?>

                             <!-- Products Name Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Products Name <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pname" name="pname" value="<?php if($this->session->flashdata('fpname')){ echo $this->session->flashdata('fpname');}else{echo $Product['pname'];}?>" placeholder="Product Name" class="form-control">

                                        <small><span class="error_txt" id="error_pname"><?php if($this->session->flashdata('error_pname')){echo $this->session->flashdata('error_pname');} ?></span></small>
                                    </div>
                                </div>

                                <!-- Products Title Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Products Category <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pcateg" name="pcateg" value="<?php if($this->session->flashdata('fpcateg')){ echo $this->session->flashdata('fpcateg');}else{echo $Product['pcatege'];}?>" placeholder="Products Category" class="form-control">

                                         <small><span class="error_txt" id="error_pcateg"><?php if($this->session->flashdata('error_pcateg')){echo $this->session->flashdata('error_pcateg');} ?></span></small>
                                    </div>
                                </div><br>


                                <!-- Products Description Name Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Product Description  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <textarea type="text" name="descp" id="editor1" value="<?php if($this->session->flashdata('fpdesc')){ echo $this->session->flashdata('fpdesc');}else{echo $Product['pdescrip'];}?>" placeholder="Descriptions...." class="form-control"><?php if($this->session->flashdata('fpdesc')){ echo $this->session->flashdata('fpdesc');}?><?php if($this->session->flashdata('fpdesc')){ echo $this->session->flashdata('fpdesc');}else{echo $Product['pdescrip'];}?></textarea>
                                        <small><span class="error_txt" id="error_fdescp"><?php if($this->session->flashdata('error_fdescp')){echo $this->session->flashdata('error_fdescp');} ?></span></small>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <!--- Content for next section --->
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body card-block">
                            
                            <!--  Product price Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Products Price <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="pprice" name="pprice" value="<?php if($this->session->flashdata('fpprice')){ echo $this->session->flashdata('fpprice');}else{echo $Product['pprice'];}?>" placeholder=" Products Price" class="form-control">

                                         <small><span class="error_txt" id="error_pprice"><?php if($this->session->flashdata('error_pprice')){echo $this->session->flashdata('error_pprice');} ?></span></small>
                                    </div>
                                </div>

                                <!--  Products Quantaty Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label> Products Quantaty <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="quantaty" name="quantaty" value="<?php if($this->session->flashdata('fquantaty')){ echo $this->session->flashdata('fquantaty');}else{echo $Product['pquantaty'];}?>" placeholder=" Products Quantaty" class="form-control">

                                         <small><span class="error_txt" id="error_quantaty"><?php if($this->session->flashdata('error_quantaty')){echo $this->session->flashdata('error_quantaty');} ?></span></small>
                                    </div>
                                </div>
                                      
                                <!-- Banner Image  Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Banner Image  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="file" id="Bimg" name="Bimg" value="" placeholder="Image " class="form-control">
                                         <small><span class="error_txt" id="error_bimage"><?php if($this->session->flashdata('error_bimage')){echo $this->session->flashdata('error_bimage');} ?></span></small>
                                         <!-- <img class="img-fluid" src="<?php echo base_url('assets/images/retreats/Banner/').$Product['bimage'];?>" class="img-responsive" width="50%"> -->
                                    </div>
                                </div>

                                <!-- Thumb Image  Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Thumb Image  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="file" id="Timg" name="Timg" value="" placeholder="Image " class="form-control">
                                         <small><span class="error_txt" id="error_ttimage"><?php if($this->session->flashdata('error_ttimage')){echo $this->session->flashdata('error_ttimage');} ?></span></small>
                                         <small><p><i>Supported: PNG / JPG<br>Size : Max 2mb<br>image dimension should be 255 * 250 pixels</i></p></small>
                                         <!-- <img class="img-fluid" src="<?php echo base_url('assets/images/retreats/Thumb/').$Product['timage'];?>" class="img-responsive" width="50%"> -->
                                    </div>
                                </div>

                                 <!-- Main Image  Field -->
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label>Main Image  <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input type="file" id="Mimg" name="Mimg" value="" placeholder="Image " class="form-control">
                                         <small><span class="error_txt" id="error_tmimage"><?php if($this->session->flashdata('error_tmimage')){echo $this->session->flashdata('error_tmimage');} ?></span></small>
                                         <small><p><i>Supported: PNG / JPG<br>Size : Max 2mb<br>image dimension should be 350 * 400 pixels</i></p></small>
                                         <!-- <img class="img-fluid" src="<?php echo base_url('assets/images/retreats/Main/').$Product['mimage'];?>" class="img-responsive" width="50%"> -->
                                    </div>
                                </div>

                            <!-- Products Status Field -->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>Status <span style="color: red;font-size: 20px;">*</span></label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control input-sm" id="status" name="status">
                                            <option value selected>--Select Status--</option>
                                            <option value="Active" <?php if($this->session->flashdata('fstatus')=='Active' || $Product['status'] == 'Active'){echo 'selected';}?>>Active</option>
                                            <option value="Deactive" <?php if($this->session->flashdata('fstatus')=='Deactive' || $Product['status'] == 'Deactive'){echo 'selected';}?>>Deactive</option>
                                        </select>
                                        <small><span class="error_txt" id="error_fstatus"><?php if($this->session->flashdata('error_fstatus')){echo $this->session->flashdata('error_fstatus');} ?></span></small>
                                    </div>
                                </div>
                            </div>

                            <!-- Save & Cancel Button -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- Cancel Button -->
                                    <a href="<?php echo base_url('Products/')?>"><button type="button" id="canceluser" class="btn btn-default pull-right btn-sm"><span class="fa fa-times"></span> Cancel</button></a>
                                    <span class="pull-right">&nbsp;</span>

                                    <!-- Save Button -->
                                    <button type="submit" class="btn btn-success btn-sm pull-right"><span class="fa fa-save"></span> Save</button>
                                </div>
                            </div>
                            <?php echo form_close() ?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Include Table Fixed Header CSS -->
            <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">   
        </div>
        <!-- /.content -->

        <div class="clearfix"></div>
    </div>

    <!-- CKEditer -->
    <script type="text/javascript" src="//cdn.ckeditor.com/4.5.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
        window.onload=function(){
    
            // instance, using default configuration.
            CKEDITOR.editorConfig = function (config) {
                config.language = 'es';
                config.uiColor = '#F7B42C';
                config.height = 200;
                config.toolbarCanCollapse = true;

            };
            CKEDITOR.replace('editor1');

        }
    </script>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <!-- <script src="</?php echo base_url();?>assets/js/common.js"></script> -->
</body>
</html>
