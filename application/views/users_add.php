<!doctype html>
<html class="no-js" lang="">
    <meta charset="utf-8">
    <title>Vyavahar - Users</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Include Table Fixed Header CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">
</head>

<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default" style="background-color: #ffffff;border-color: #ffffff;">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">

                  <li class="active">
                        <a style="color: black" href="<?php echo base_url('Dashboard');?>" ><i class="fa fa-home fa-fw fa-2x"></i>Dashboard</a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Orders');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Orders </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Packing Materials </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Returns </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Products');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Products </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Inventory');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Inventory </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Statement');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Statement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="#"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Invoice </a>
                    </li>

                    <li class="">
                        <a style="color: black" href=""><i class="fa fa-product-hunt fa-fw fa-2x"></i>Advertisement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Logs');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Logs </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Users');?>"><i class="fa fa-user-plus fa-fw fa-2x"></i>Users </a>
                    </li>    
                </ul>
            </div>
        </nav>
    </aside>

    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="img-responsive navbar-brand" href="./"><img src="<?php echo base_url(); ?>assets/images/profile.jpeg" alt="Logo"></a>
                    <a class="img-responsive navbar-brand hidden" href="./"><img src="<?php echo base_url(); ?>assets/images/profile.jpeg" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">

                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="img-responsive user-avatar rounded-circle" src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?php echo base_url('Profile');?>"><i class="fa fa- user"></i>My Profile</a>

                            <a class="nav-link" href="<?php echo base_url('Login/Logout');?>"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <!-- Content -->
        <div class="content">

            <!--Div to show top error message-->
            <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>
            <!--Div to show top error message-->
            <div id="result"><?php if($this->session->flashdata('result')){echo $this->session->flashdata('result');} ?></div>
            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>

            <div class="row">

                 <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <h4 style="font-weight: 600"><?php echo $title;?></h4>
                </div>

                <div class="visible-sm"><br></div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body card-block">
                            <!--form open-->
                            <!-- </?php echo form_open('Quotes/CreateNew')?> -->
                            <?php echo form_open_multipart(base_url('Users/CreateUser'), array('class'=>'UploadSignature', 'id'=>'UploadSignatureForm')); ?>
                                
                                <!-- Master Name Field -->
                                <div class="row form-group">
                                    <div class="col-sm-3">
                                        <label>Name *</label>
                                     </div>
                                     <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input type="text" id="fname" name="fname" value="<?php if($this->session->flashdata('fdfname')){echo $this->session->flashdata('fdfname'); }?>" class="AlphaNumericSpaces Char16 form-control input-sm" placeholder="First Name">
                                                <small><span class="error_txt" id="error_fname"><?php if($this->session->flashdata('error_fname')){echo $this->session->flashdata('error_fname');} ?></span></small>
                                            </div>
                                            <div class="visible-xs"><br></div>
                                            <div class="col-sm-6">
                                                <input type="text" id="lname" name="lname" value="<?php if($this->session->flashdata('fdlname')){echo $this->session->flashdata('fdlname'); }?>" class="AlphaNumericSpaces Char16 form-control input-sm" placeholder="Last Name"> 
                                                <small><span class="error_txt" id="error_lname"><?php if($this->session->flashdata('error_lname')){echo $this->session->flashdata('error_lname');} ?></span></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-sm-3">
                                        <label>User ID *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" id="userid" value="<?php if($this->session->flashdata('fduid')){echo $this->session->flashdata('fduid'); }else{echo 'email@example.com';}?>" name="userid" class="Char64 form-control input-sm" placeholder="email@example.com">
                                        <small><span class="error_txt" id="error_userid"><?php if($this->session->flashdata('error_userid')){echo $this->session->flashdata('error_userid');} ?></span></small>
                                    </div>
                                </div>
                                
                                <div class="row form-group">
                                    <div class="col-sm-3">
                                        <label>Password *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="Password" id="password" name="password" value="<?php if($this->session->flashdata('fdpwd')){echo $this->session->flashdata('fdpwd'); } ?>" class="AlphaNumericSpaces Char64 form-control input-sm" placeholder="Password">
                                        <small><span class="error_txt" id="error_password"><?php if($this->session->flashdata('error_password')){echo $this->session->flashdata('error_password');} ?></span></small>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Role *</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control input-sm" id="role" name="role">
                                                <option value="-">--Select Role--</option>
                                                <option value="SuperAdmin" <?php if($this->session->flashdata('fdrole')=='SuperAdmin'){echo 'selected';}?>>Super Administrator</option>
                                                <option value="Admin" <?php if($this->session->flashdata('fdrole')=='Admin'){echo 'selected';}?>>Admin</option>
                                                <option value="Operator" <?php if($this->session->flashdata('fdrole')=='Operator'){echo 'selected';}?>>Operator</option>
                                            </select>
                                            <small><span class="error_txt" id="error_role"><?php if($this->session->flashdata('error_role')){echo $this->session->flashdata('error_role');} ?></span></small>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-sm-3">
                                        <label>Mobile *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" id="mobile" name="mobile" value="<?php if($this->session->flashdata('fdphone')){echo $this->session->flashdata('fdphone'); }?>" class="IntegerOnly Char10 form-control input-sm" placeholder="Mobile Number"> 
                                        <small><span class="error_txt" id="error_mobile"><?php if($this->session->flashdata('error_mobile')){echo $this->session->flashdata('error_mobile');} ?></span></small>
                                            
                                    </div>
                                </div>

                                <br><br>
                                <div class="row">
                                    <div class="col-sm-12">

                                        <!-- Cancel Button -->
                                        <a href="<?php echo base_url('Users')?>"><button type="button" id="canceluser" class="btn btn-default pull-right btn-sm"><span class="fa fa-times"></span> Cancel</button></a>
                                        <span class="pull-right">&nbsp;</span>

                                        <!-- Save Button -->
                                        <button type="submit" class="btn btn-success btn-sm pull-right"><span class="fa fa-save"></span> Save</button>
                                    </div>
                                </div>
                            <?php echo form_close() ?>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Include Table Fixed Header CSS -->
            <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">
            
        </div>
        <!-- /.content -->
        <div class="clearfix"></div>
        <!-- Footer -->
        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <!-- <div class="col-sm-12">
                        <p style="text-align: center;">No Copyright. Please spread the message of Anapanasati Meditation, Vegetarianism and Spiritual Science to the whole world.</p>
                        <p style="text-align: center;">Designed by <a href="https://www.puremindz.com/" target="_blank"><b style="color: blue">Puremindz</b></a></p>
                    </div> -->
                </div>
            </div>
        </footer>
    </div>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
</body>
</html>
