 <?php include APPPATH.'views/include/header.php';?>
 

    <style type="text/css">
        .SearchBox {
            width: 100%;
            height: 40px;
            padding: 0px 10px;
            margin-bottom: 10px;
            border: none;
            font-family: 'Open Sans', sans-serif;
        }
    </style>
      <div class="row">
        

            <!--Div to show top error message-->
            <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>

            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>

            <div class="row">
                 <div class="col-sm-1.75" style="padding:10px;border:1px solid #ddd; border-radius:20%;height: 100px;margin: 10px;" >
                    <p>Total Buyers</p>    
                    <div class="card" style="margin-bottom:0px !important">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:05px; height:10px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"><?=GetcountBuyerAll();?></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-1.75" style="padding:10px;border:1px solid #ddd; border-radius:20%;height: 100px;margin: 10px;" >
                    <p>New Buyers</p>    
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:05px; height:10px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-1.75" style="padding:10px;border:1px solid #ddd; border-radius:20%;height: 100px;margin: 10px;" >   
                    <p>Register Buyers</p>    
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:05px; height:10px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-1.75" style="padding:10px;border:1px solid #ddd; border-radius:20%;height: 100px;margin: 10px;">
                    <p>Draft Buyers</p>    
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:05px; height:10px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-1.75" style="padding:10px;border:1px solid #ddd; border-radius:20%;height: 100px;margin: 10px;" >
                    <p>Decline Buyers</p>    
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:05px; height:10px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>    
                
                
				<div class="col-md-12">
                <div class="col-md-4">
                    <h4 style="font-weight: 600"><?php echo $title;?></h4>
                </div>

                <div class="visible-sm"><br></div>

                <div class="col-sm-4" style="margin-top: 5px; text-align: right;">
                    <a href="<?php echo base_url('Buyer/AddBuyer'); ?>"><button class="btn btn-primary btn-sm btn-primary" name="NewPayment"><span class="glyphicon glyphicon-plus"></span> Create Buyer</button></a>
                </div>

                <div class="visible-sm"><br></div>

                <div class="col-sm-4">
                    <input id="SearchBox" type="text" name="search" class="SearchBox " placeholder="Search..">
                </div>
                </div> 
            </div>
			
			 <div class="table-responsive">
				<table class="table mb-0 table-bordered" id="CommonTable">
					<thead class="thead-light">
					
						<tr>
						   
							 <th class="" style="cursor:pointer;"> ID
							</th>
							<th class="" style="cursor:pointer;">Fist Name
							</th>
							<th class="" style="cursor:pointer;">Last Name
							</th>
							<th class="" style="cursor:pointer;">Status
							</th>  
							 <th>Action</th>  
						</tr>
					</thead>
					<tbody id="SearchData">
					<?php $i=0; foreach ($EditProducts->result() as $row){?>
					<tr>

                                <?php $ID = $row->id; 
                                if($ID<=9){$ID='000'.$ID;}
                                if($ID>9 && $ID<=99){$ID='00'.$ID;}
                                if($ID>99 && $ID<=999){$ID='0'.$ID;} ?>

                                 <td><?php echo $ID; ?></td>
                                <td><?php echo $row->fname; ?></td>
                                <td><?php echo $row->lname; ?></td>
                                <td>
                                    <?php 
                                        #Assigning the status to Variable to select the color
                                        switch($row->status){
                                            case 'Approved':
                                                $StatusColor='success';
                                                break;
                                            case 'Deactive':
                                                $StatusColor='danger';
                                                break;
                                            default:
                                                $StatusColor='default';
                                                break;
                                        }
                                    ?>
                                    <span class='label label-<?php echo $StatusColor; ?>'><?php echo $row->status; ?></span>
                                </td>
                                
                                <!-- More Information Button -->
                                <td>
								  <div class="dropdown">
												<button class="btn" type="button" data-toggle="dropdown">
												<span class="caret"></span></button>
												<ul class="dropdown-menu">
												  
												  <li><a class="dropdown-item" href="<?php echo base_url('Buyer/EditBuyer/').$row->id; ?>">Edit</a></li>  
												</ul>
											  </div>
											</td>
									
									
                                 
										</tr>
										<?php $i++; } ?>
							
								</tbody>
                        </table>
                                      
                   </div>
									
				
               
            <br>    
            <!-- Display number of users -->
            <div class="col-md-12 row">
                <div class="col-sm-3"><strong>No. of Users :</strong> <?php echo $i; ?> </div>
                <div class="col-sm-9"></div>
            </div>           
        </div>
        <!-- /.content -->

        <div class="clearfix"></div>

    </div>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    
    <!-- Search Data -->
    <script type="text/javascript">
        $(document).ready(function(){
            $("#SearchBox").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#SearchData tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });

        
        $(document).on('click', 'th', function() {
            var table = $(this).parents('table').eq(0);
            var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()));
            this.asc = !this.asc;
            if (!this.asc) {
                rows = rows.reverse();
            }
            table.children('tbody').empty().html(rows);
        });

        function comparer(index) {
            return function(a, b) {
                var valA = getCellValue(a, index),
                valB = getCellValue(b, index);
                return $.isNumeric(valA) && $.isNumeric(valB) ?
                valA - valB : valA.localeCompare(valB);
            };
        }

        function getCellValue(row, index) {
            return $(row).children('td').eq(index).text();
        }

        // Menu Trigger
        $('#menuToggle').on('click', function(event) {
            var windowWidth = $(window).width();         
            if (windowWidth<1010) { 
                $('body').removeClass('open'); 
                if (windowWidth<760){ 
                    $('#left-panel').slideToggle(); 
                } else {
                    $('#left-panel').toggleClass('open-menu');  
                } 
            } else {
                $('body').toggleClass('open');
                $('#left-panel').removeClass('open-menu');  
            } 
                 
        }); 

         
        $(".menu-item-has-children.dropdown").each(function() {
            $(this).on('click', function() {
                var $temp_text = $(this).children('.dropdown-toggle').html();
                $(this).children('.sub-menu').prepend('<li class="subtitle">' + $temp_text + '</li>'); 
            });
        });

            // Load Resize 
        $(window).on("load resize", function(event) { 
            var windowWidth = $(window).width();         
            if (windowWidth<1010) {
                $('body').addClass('small-device'); 
            } else {
                $('body').removeClass('small-device');  
            } 
            
        });

        $.noConflict();

    </script>
        
      </div>
    </div>
  </main>
  <!-- page-content" -->
</div>
<!-- page-wrapper -->
