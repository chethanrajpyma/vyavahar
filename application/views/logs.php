 <!doctype html>
<html class="no-js" lang="">
    <meta charset="utf-8">
    <title>Vyavahar -  Logs</title>

    <link rel="icon" href="<?php echo base_url(); ?>assets/images/logo.jpeg">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Include Table Fixed Header CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">

    <style type="text/css">
        .SearchBox {
            width: 100%;
            height: 40px;
            padding: 0px 10px;
            margin-bottom: 10px;
            border: none;
            font-family: 'Open Sans', sans-serif;
        }
    </style>
</head>

<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default" style="background-color: #ffffff;border-color: #ffffff;">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">

                  <li class="active">
                        <a style="color: black" href="<?php echo base_url('Dashboard');?>" ><i class="fa fa-home fa-fw fa-2x"></i>Dashboard</a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Orders');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Orders </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Packing Materials </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Returns </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Products');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Products </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Inventory');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Inventory </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Statement');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Statement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="#"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Invoice </a>
                    </li>

                    <li class="">
                        <a style="color: black" href=""><i class="fa fa-product-hunt fa-fw fa-2x"></i>Advertisement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Logs');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Logs </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Users');?>"><i class="fa fa-user-plus fa-fw fa-2x"></i>Users </a>
                    </li>   
                </ul>
            </div>
        </nav>
    </aside>
    <!-- /#left-panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                     <a class="img-responsive navbar-brand" href="#"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo"></a> 

                    <a class="img-responsive navbar-brand hidden" href="./"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo" width="50%"></a>

                        <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>

            <div class="top-right">
                <div class="header-menu">
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="img-responsive user-avatar rounded-circle" src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?php echo base_url('Profile');?>"><i class="fa fa- user"></i>My Profile</a>

                            <a class="nav-link" href="<?php echo base_url('Login/Logout');?>"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- Content -->
        <div class="content">

            <!--Div to show top error message-->
            <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>

            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>

            <div class="row">
                <div class="col-sm-4">
                    <h4 style="font-weight: 600"><?php echo $title;?></h4>
                </div>

                <div class="visible-sm"><br></div>

                <div class="visible-sm"><br></div>

                <div class="col-sm-4">
                    <input id="SearchBox" type="text" name="search" class="SearchBox " placeholder="Search..">
                </div>
                
            </div>

            <section>
                <div style="width:100%; height:55vh; border-top:3px SOLID LIGHTGREY; overflow-y:scroll; overflow-x:scroll;">
                    <table id="CommonTable" class="TblFont">
                        <thead class="hidden-xs hidden-sm">
                           <tr>
                                <th>Log #<div>Log #</div></th>
                                <th>Date
                                    <div>Date<a href=""><span class="glyphicon glyphicon-triangle-bottom"></span></a>
                                    <a href=""><span class="glyphicon glyphicon-triangle-top"></span></a></div>
                                </th>
                                <th>IP Address
                                    <div>IP Address<a href=""><span class="glyphicon glyphicon-triangle-bottom"></span></a>
                                    <a href=""><span class="glyphicon glyphicon-triangle-top"></span></a></div>
                                </th>
                                <th>User ID
                                    <div>User ID<a href=""><span class="glyphicon glyphicon-triangle-bottom"></span></a>
                                    <a href=""><span class="glyphicon glyphicon-triangle-top"></span></a></div>
                                </th>
                                <th>Msg
                                    <div>Msg<a href=""><span class="glyphicon glyphicon-triangle-bottom"></span></a>
                                    <a href=""><span class="glyphicon glyphicon-triangle-top"></span></a></div>
                                </th>
                                <th>Details
                                    <div>Details<a href=""><span class="glyphicon glyphicon-triangle-bottom"></span></a>
                                    <a href=""><span class="glyphicon glyphicon-triangle-top"></span></a></div>
                                </th>
                                <th>Category
                                    <div>Category<a href=""><span class="glyphicon glyphicon-triangle-bottom"></span></a>
                                    <a href=""><span class="glyphicon glyphicon-triangle-top"></span></a></div>
                                </th>
                          </tr>
                        </thead>

                        <tbody id="SearchData">
                            <tr class="visible-xs visible-sm">
                                <td style="cursor:pointer;" ><b class="pull-right"> ID</b></td>
                                <td style="cursor:pointer;"><b>Date</b></td>
                                <td style="cursor:pointer;"><b>IP Address </b></td>
                                <td style="cursor:pointer;"><b>User ID </b></td>
                                <td style="cursor:pointer;"><b> Msg </b></td>
                                <td style="cursor:pointer;"><b>Details </b></td>
                                <td style="cursor:pointer;">Category</td>
                            </tr>

                             <!-- <?php $i=0; foreach ($LogsData->result() as $row){?> -->
                            <tr>

                                <?php $ID = $row->clid; 
                                if($ID<=9){$ID='000'.$ID;}
                                if($ID>9 && $ID<=99){$ID='00'.$ID;}
                                if($ID>99 && $ID<=999){$ID='0'.$ID;} ?>

                                        <tr>
                                            <td style="padding-left:20px;"><?php echo $row->clid; ?></td>
                                            <td><?php echo $row->date; ?></td>
                                            <td><?php echo $row->ip; ?></td>
                                            <td><?php echo $row->uid; ?></td>
                                            <td><?php echo $row->logmsg; ?></td>
                                            <td><?php echo $row->details; ?></td>
                                            <td><?php echo $row->logcat; ?></td>
                                        </tr>
                                
                                <!-- More Information Button -->
                                 <td>
                                    <a href="<?php echo base_url('Logs/').$row->clid; ?>">
                                <span class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Edit" data-placement="left"></span></a></td>
                        
                            </tr>
                            <?php $i++; } ?>
                        </tbody>
                    </table>
                </div>
            </section>
            <br>    
            <!-- Display number of users -->
            <div class="row">
                <div class="col-sm-3"><strong>No. of Users :</strong> <?php echo $i; ?> </div>
                <div class="col-sm-9"></div>
            </div>           
        </div>
        <!-- /.content -->

        <div class="clearfix"></div>

    </div>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    
    <!-- Search Data -->
    <script>
    $("#sdate").change(function(){
        window.location.assign("<?php echo base_url().'Logs/Sort/'.$SField; ?>/" + $("#sdate").val());
    });

    $(function() {
        $( "#sdate" ).datepicker({
            minDate: -15,
            maxDate: "+0M +0D",
            dateFormat: 'yy-mm-dd'
        });
    });
    
    $(document).ready(function(){
      $("#SearchBox").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#LogData tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
</script>
</body>
</html>
