 <!doctype html>
<html class="no-js" lang="">
    <meta charset="utf-8">
    <title>Vyavahar -  Products</title>

    <link rel="icon" href="<?php echo base_url(); ?>assets/images/logo.jpeg">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Include Table Fixed Header CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">

    <style type="text/css">
        .SearchBox {
            width: 100%;
            height: 40px;
            padding: 0px 10px;
            margin-bottom: 10px;
            border: none;
            font-family: 'Open Sans', sans-serif;
        }
    </style>
</head>

<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default" style="background-color: #ffffff;border-color: #ffffff;">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">

                   <li class="active">
                        <a style="color: black" href="<?php echo base_url('Dashboard');?>" ><i class="fa fa-home fa-fw fa-2x"></i>Dashboard</a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Orders');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Orders </a>
                    </li> 
					
					<li>
          
			
			
					<li class="">
                        <a style="color: black" href="<?php echo base_url('Products');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Buyers </a>
                    </li>
					 class='nav-item dropdown'>
                <a class='nav-link dropdown-toggle' href="" role='button' id="dropdownCadastro"
                   data-toggle='dropdown' aria-haspopup="true" aria-expanded="false">
                    Projeto
                </a>
                <div class="dropdown-menu shadow border-0" aria-labelledby="dropdownCadastro">
                <a class="dropdown-item js-scroll-trigger" href='<?php echo base_url('projeto');?> '>Cadastrar Projeto</a>
               <a class="dropdown-item js-scroll-trigger" href='<?php echo base_url('avaliacao');?> '>Avaliar Projeto</a>
                <a class="dropdown-item js-scroll-trigger" href='<?php echo base_url('consultarProjetoServidor');?> '>Consultar Projeto</a>
                </div>
            </li>
                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Packing Materials </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Returns </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Products');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Products </a>
                    </li>
					
					
					
					
                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Inventory');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Inventory </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Statement');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Statement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="#"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Invoice </a>
                    </li>

                    <li class="">
                        <a style="color: black" href=""><i class="fa fa-product-hunt fa-fw fa-2x"></i>Advertisement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Logs');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Logs </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Users');?>"><i class="fa fa-user-plus fa-fw fa-2x"></i>Users </a>
                    </li> 
					 class='nav-item dropdown'>
                <a class='nav-link dropdown-toggle' href="" role='button' id="dropdownCadastro"
                   data-toggle='dropdown' aria-haspopup="true" aria-expanded="false">
                    Projeto
                </a>
                <div class="dropdown-menu shadow border-0" aria-labelledby="dropdownCadastro">
                <a class="dropdown-item js-scroll-trigger" href='<?php echo base_url('projeto');?> '>Cadastrar Projeto</a>
               <a class="dropdown-item js-scroll-trigger" href='<?php echo base_url('avaliacao');?> '>Avaliar Projeto</a>
                <a class="dropdown-item js-scroll-trigger" href='<?php echo base_url('consultarProjetoServidor');?> '>Consultar Projeto</a>
                </div>
            </li>
     <a href="#contact">Search</a>
 </div>  
                </ul>
            </div>
        </nav>
    </aside>
    <!-- /#left-panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                     <a class="img-responsive navbar-brand" href="#"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo"></a> 

                    <a class="img-responsive navbar-brand hidden" href="./"><img src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo" width="50%"></a>

                        <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>

            <div class="top-right">
                <div class="header-menu">

                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="img-responsive user-avatar rounded-circle" src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo">
                        </a>


                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?php echo base_url('Profile');?>"><i class="fa fa- user"></i>My Profile</a>

                            <a class="nav-link" href="<?php echo base_url('Login/Logout');?>"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <!-- Content -->
        <div class="content">

            <!--Div to show top error message-->
            <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>

            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>

            <div class="row">
                 <div class="col-sm-1.75" style="padding:10px" >
                    <p>Total Buyers</p>    
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:05px; height:10px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"><?=GetcountOrdersAll();?></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-1.75" style="padding:10px" >
                    <p>New Buyers</p>    
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:05px; height:10px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-1.75" style="padding:10px" >   
                    <p>Register Buyers</p>    
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:05px; height:10px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-1.75" style="padding:10px" >
                    <p>Draft Buyers</p>    
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:05px; height:10px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-1.75" style="padding:10px" >
                    <p>Decline Buyers</p>    
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:05px; height:10px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>    
                
                
				<div class="col-md-12">
                <div class="col-md-4">
                    <h4 style="font-weight: 600"><?php echo $title;?></h4>
                </div>

                <div class="visible-sm"><br></div>

                <div class="col-sm-4" style="margin-top: 5px; text-align: right;">
                    <a href="<?php echo base_url('Buyer/AddProducts'); ?>"><button class="btn btn-primary btn-sm btn-primary" name="NewPayment"><span class="glyphicon glyphicon-plus"></span> Create Buyer</button></a>
                </div>

                <div class="visible-sm"><br></div>

                <div class="col-sm-4">
                    <input id="SearchBox" type="text" name="search" class="SearchBox " placeholder="Search..">
                </div>
                </div> 
            </div>

            <section>
                <div style="width:100%; height:55vh; border-top:3px SOLID LIGHTGREY; overflow-y:scroll; overflow-x:scroll;">
                    <table id="CommonTable" class="TblFont">
                        <thead class="hidden-xs hidden-sm">
                            <tr>
                                <th class="nofocus" style="cursor:pointer;"> ID<div> ID <i class="fa fa-sort"></i></div>
                                </th>
                                <th class="nofocus" style="cursor:pointer;">Product<div> Product <i class="fa fa-sort"></i></div>
                                </th>
                                <th class="nofocus" style="cursor:pointer;">Category<div> Category <i class="fa fa-sort"></i></div>
                                <th class="nofocus" style="cursor:pointer;">Product Quantaty<div> Product Quantaty <i class="fa fa-sort"></i></div>
                                </th>
                                <th class="nofocus" style="cursor:pointer;">Status<div>Status <i class="fa fa-sort"></i></div>
                                </th>
                               <!--  <th class="nofocus" style="cursor:pointer;">Created By<div>Created By <i class="fa fa-sort"></i></div>
                                </th> -->
                            </tr>
                        </thead>

                        <tbody id="SearchData">
                            <tr class="visible-xs visible-sm">
                                <td style="cursor:pointer;" ><b class="pull-right"> ID</b></td>
                                <td style="cursor:pointer;"><b>Product</b></td>
                                <td style="cursor:pointer;"><b>Category </b></td>
                                <td style="cursor:pointer;"><b>Products Quantaty </b></td>
                                <td style="cursor:pointer;">Status</td>
                            </tr>

                            <?php $i=0; foreach ($EditProducts->result() as $row){?>
                            <tr>

                                <?php $ID = $row->pid; 
                                if($ID<=9){$ID='000'.$ID;}
                                if($ID>9 && $ID<=99){$ID='00'.$ID;}
                                if($ID>99 && $ID<=999){$ID='0'.$ID;} ?>

                                 <td><?php echo $ID; ?></td>
                                <td><?php echo $row->pname; ?></td>
                                <td><?php echo $row->pcatege; ?></td>
                                <td><?php echo $row->pquantaty; ?></td>
                                <td><?php echo $row->status; ?></td>
                                <td>
                                    <?php 
                                        #Assigning the status to Variable to select the color
                                        switch($row->status){
                                            case 'Approved':
                                                $StatusColor='success';
                                                break;
                                            case 'Deactive':
                                                $StatusColor='danger';
                                                break;
                                            default:
                                                $StatusColor='default';
                                                break;
                                        }
                                    ?>
                                    <span class='label label-<?php echo $StatusColor; ?>'><?php echo $row->status; ?></span>
                                </td>
                                
                                <!-- More Information Button -->
                                <td>
                                    <a href="<?php echo base_url('Products/EditProducts/').$row->pid; ?>">
                                <span class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Edit" data-placement="left"></span></a></td>
                            
                            </tr>
                            <?php $i++; } ?>
                        </tbody>
                    </table>
                </div>
            </section>
            <br>    
            <!-- Display number of users -->
            <div class="row">
                <div class="col-sm-3"><strong>No. of Users :</strong> <?php echo $i; ?> </div>
                <div class="col-sm-9"></div>
            </div>           
        </div>
        <!-- /.content -->

        <div class="clearfix"></div>

    </div>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    
    <!-- Search Data -->
    <script type="text/javascript">
        $(document).ready(function(){
            $("#SearchBox").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#SearchData tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });

        
        $(document).on('click', 'th', function() {
            var table = $(this).parents('table').eq(0);
            var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()));
            this.asc = !this.asc;
            if (!this.asc) {
                rows = rows.reverse();
            }
            table.children('tbody').empty().html(rows);
        });

        function comparer(index) {
            return function(a, b) {
                var valA = getCellValue(a, index),
                valB = getCellValue(b, index);
                return $.isNumeric(valA) && $.isNumeric(valB) ?
                valA - valB : valA.localeCompare(valB);
            };
        }

        function getCellValue(row, index) {
            return $(row).children('td').eq(index).text();
        }

        // Menu Trigger
        $('#menuToggle').on('click', function(event) {
            var windowWidth = $(window).width();         
            if (windowWidth<1010) { 
                $('body').removeClass('open'); 
                if (windowWidth<760){ 
                    $('#left-panel').slideToggle(); 
                } else {
                    $('#left-panel').toggleClass('open-menu');  
                } 
            } else {
                $('body').toggleClass('open');
                $('#left-panel').removeClass('open-menu');  
            } 
                 
        }); 

         
        $(".menu-item-has-children.dropdown").each(function() {
            $(this).on('click', function() {
                var $temp_text = $(this).children('.dropdown-toggle').html();
                $(this).children('.sub-menu').prepend('<li class="subtitle">' + $temp_text + '</li>'); 
            });
        });

            // Load Resize 
        $(window).on("load resize", function(event) { 
            var windowWidth = $(window).width();         
            if (windowWidth<1010) {
                $('body').addClass('small-device'); 
            } else {
                $('body').removeClass('small-device');  
            } 
            
        });

        $.noConflict();

    </script>
</body>
</html>
