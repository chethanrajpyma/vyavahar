<!doctype html>
<html class="no-js" lang="">
    <meta charset="utf-8">
    <title>Vyavahar - Users</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Include Table Fixed Header CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table-header.css'); ?>">

</head>

<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default" style="background-color: #ffffff;border-color: #ffffff;">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">

                   <li class="active">
                        <a style="color: black" href="<?php echo base_url('Dashboard');?>" ><i class="fa fa-home fa-fw fa-2x"></i>Dashboard</a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Orders');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Orders </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Packing Materials </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Returns');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Returns </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Products');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Products </a>
                    </li>

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Inventory');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Inventory </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Statement');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Statement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="#"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Invoice </a>
                    </li>

                    <li class="">
                        <a style="color: black" href=""><i class="fa fa-product-hunt fa-fw fa-2x"></i>Advertisement </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Logs');?>"><i class="fa fa-product-hunt fa-fw fa-2x"></i>Logs </a>
                    </li> 

                    <li class="">
                        <a style="color: black" href="<?php echo base_url('Users');?>"><i class="fa fa-user-plus fa-fw fa-2x"></i>Users </a>
                    </li>   
                </ul>
            </div>
        </nav>
    </aside>

    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="img-responsive navbar-brand" href="./"><img src="<?php echo base_url(); ?>assets/images/profile.jpeg" alt="Logo"></a>
                    <a class="img-responsive navbar-brand hidden" href="./"><img src="<?php echo base_url(); ?>assets/images/profile.jpeg" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">

                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="img-responsive user-avatar rounded-circle" src="<?php echo base_url();?>assets/images/profile.jpeg" alt="Logo">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="<?php echo base_url('Profile');?>"><i class="fa fa- user"></i>My Profile</a>

                            <a class="nav-link" href="<?php echo base_url('Login/Logout');?>"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>

                </div>
            </div>
        </header>

        <!-- Content -->
        <div class="content">

            <!--Div to show top error message-->
            <div><span class="error_txt" id="errors"><?php if($this->session->flashdata('errors')){echo $this->session->flashdata('errors');} ?></span></div>
            <!--Div to show top error message-->
            <div id="result"><?php if($this->session->flashdata('result')){echo $this->session->flashdata('result');} ?></div>
            <!--Div to show top error message-->
            <div ><?php if($this->session->flashdata('SuccessMsg')){echo $this->session->flashdata('SuccessMsg');} ?></div>

            <div class="row">
                <div class="col-sm-12">
                    <h4 style="font-weight: 600"><?php echo $title;?></h4>
                </div>

                <div class="visible-sm"><br></div> 
            </div>

            <div class="row">
                 <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="card">
                        <div class="card-body card-block">
                            <div class="col-sm-12" style="width:50%; height:50px;">
                                <p style="text-align: justify;font-size: 20px;font-weight: bold;line-height: 1.5em;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body card-block">
                            <fieldset>
                                <!-- Password Reset Fields -->
                                <legend>User Details:</legend>
                            <!--form open-->
                            <?php echo form_open_multipart(base_url('Users/EditUser'), array('class'=>'UploadSignature', 'id'=>'UploadSignatureForm')); ?>
                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- Back Button -->
                                        <a href="<?php echo base_url('Users');?>"><button type="button" id="" class="btn btn-default pull-right btn-sm"><span class="glyphicon glyphicon-chevron-left"></span> Back</button></a><span class="pull-right">&nbsp;</span>
                                        
                                        <!-- Edit Button -->
                                        <button type="button" id="deleteuser" class="btn btn-danger pull-right btn-sm"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                        <span class="pull-right">&nbsp;</span>
                                        <button type="button" id="edituser" class="btn btn-warning pull-right btn-sm"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                                        
                                        <!-- Cancel Button -->
                                        <button type="button" id="canceluser" class="btn btn-default pull-right btn-sm" style="display:none;"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                                        <span class="pull-right">&nbsp;</span>
                                        
                                        <!-- Save Button -->
                                        <button type="submit" id="saveuser" class="btn btn-success pull-right btn-sm" style="display:none;"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>

                                    </div>
                                </div>
                                <br>

                                <!--Name Field-->
                                <div class="row form-group">
                                    <div class="col-sm-3">
                                        <label>Name *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <input id="fname" name="fname" type="text" class="AlphaNumericSpaces Char16 form-control input-sm" placeholder="First Name" value="<?php if($this->session->flashdata('fdfname')){echo $this->session->flashdata('fdfname'); }elseif($this->session->flashdata('dflag')!='Yes'){echo $UserData['fname'];} ?>" disabled>
                                                <small><span class="error_txt" id="error_fname"><?php if($this->session->flashdata('error_fname')){echo $this->session->flashdata('error_fname');} ?></span></small>
                                            </div>
                                            <!-- Line Break in mobile view-->
                                            <div class="visible-sm visible-xs"><br></div>
                                            <div class="col-sm-6">
                                                <input id="lname" name="lname" type="text" class="AlphaNumericSpaces Char16 form-control input-sm" placeholder="Last Name" value="<?php if($this->session->flashdata('fdlname')){echo $this->session->flashdata('fdlname'); }elseif($this->session->flashdata('dflag')!='Yes'){ echo $UserData['lname'];} ?>" disabled>
                                                <small><span class="error_txt" id="error_fname"><?php if($this->session->flashdata('error_lname')){echo $this->session->flashdata('error_lname');} ?></span></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                                <!--Email Field-->
                                <div class="row form-group">
                                    <div class="col-sm-3">
                                        <label>Email *</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <?php echo $UserData['uid']; ?>
                                    </div>
                                </div>

                                <!--Role Field-->
                                <div class="row form-group">
                                    <div class="col-sm-3">
                                        <label>Role *</label>   
                                    </div>
                                    <div class="col-sm-9">
                                        <select id="role" name="role" class="form-control input-sm" disabled>
                                            <option value="SuperAdmin" <?php if($this->session->flashdata('fdrole')=='SuperAdmin'){echo 'selected';}elseif($UserData['role']=='SuperAdmin'){echo 'selected';} ?>>Super Administrator</option>
                                            <option value="Admin" <?php if($this->session->flashdata('fdrole')=='Admin'){echo 'selected';}elseif($UserData['role']=='Admin'){echo 'selected';} ?>>Admin</option>
                                            <option value="Admin" <?php if($this->session->flashdata('fdrole')=='Admin'){echo 'selected';}elseif($UserData['role']=='Operator'){echo 'selected';} ?>>Operator</option>
                                        </select>
                                    </div>
                                </div>
                                    
                                <!--Status Field-->
                                <div class="row form-group">
                                    <div class="col-sm-3">
                                        <label>Status *</label> 
                                    </div>
                                    <div class="col-sm-9"> 
                                        <select id="status" name="status" class="form-control input-sm" disabled>
                                            <option value="Active" <?php if($this->session->flashdata('fdstatus')=='Active'){echo 'selected';}elseif($UserData['status']=='Active'){echo 'selected';} ?>>Active</option>
                                            <option value="Inactive" <?php if($this->session->flashdata('fdstatus')=='Inactive'){echo 'selected';}elseif($UserData['status']=='Inactive'){echo 'selected';} ?>>Inactive</option>
                                            <option value="Leaver" <?php if($this->session->flashdata('fdstatus')=='Leaver'){echo 'selected';}elseif($UserData['status']=='Leaver'){echo 'selected'; }?>>Leaver</option>
                                        </select>
                                    </div>
                                </div>

                              
                                <div class="row form-group">
                                    <div class="col-sm-3">
                                        <label>Mobile *</label>   
                                    </div>
                                    <div class="col-sm-9">
                                        <input id="mobile" name="mobile" type="text" class="IntegerOnly Char10 form-control input-sm" placeholder="Mobile Number" value="<?php if($this->session->flashdata('fdphone')){echo $this->session->flashdata('fdphone'); }elseif($this->session->flashdata('dflag')!='Yes'){ echo $UserData['phone']; }?>" disabled>
                                       <small><span class="error_txt" id="error_fname"><?php if($this->session->flashdata('error_mobile')){echo $this->session->flashdata('error_mobile');} ?></span></small>
                                    </div>
                                </div>
                            </fieldset>
                                <fieldset>
                                    <!-- Password Reset Fields -->
                                    <legend>Reset Password:</legend>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            <label>Password <a href="#" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="bottom" data-content="<small><i>Leave blank if no changes</i></small>"><span class="glyphicon glyphicon-question-sign"></span></a></label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input id="password" name="password" type="Password" class="form-control input-sm" placeholder="Password" readonly>
                                            <small><span class="error_txt" id="error_password"><?php if($this->session->flashdata('error_password')){echo $this->session->flashdata('error_password');} ?></span></small>
                                        </div>
                                    </div>
                                </fieldset>

                            <?php echo form_close() ?>
                        </div>
                    </div>
                </div>
            </div>          
        </div>
        <!-- /.content -->
        <div class="clearfix"></div>
        <!-- Footer -->
        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                   <!--  <div class="col-sm-12">
                        <p style="text-align: center;".</p>
                        <p style="text-align: center;">Designed by <a href="Vyavahar" target="_blank"><b style="color: blue">Vyavahar</b></a></p>
                    </div> -->
                </div>
            </div>
        </footer>
    </div>
    
    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/sweetalert/sweetalert.min.js"></script>

    <script>
        var flag="<?php echo $this->session->flashdata('flag');?>";
    </script>

    <script type="text/javascript">
        $(document).ready(function() { 
            //Enables all input fields on clicking Edit Button
            $('#edituser').on('click', function(){

                //Show Save & Cancel Button on clicking Edit Button
                $( "#saveuser" ).show();
                $( "#canceluser" ).show();
                $( "#deleteuser" ).hide();
                $( "#edituser" ).hide();
                
                //Enable all text boxes and select boxes on clicking Edit Button
                $( "#fname" ).prop( "disabled", false );
                $( "#lname" ).prop( "disabled", false );
                $( "#status" ).prop( "disabled", false );
                $( "#role" ).prop( "disabled", false );
                $( "#mobile" ).prop( "disabled", false );
                $( "#password" ).prop( "readonly", false );
           
            });

            if(flag=='Yes'){

            //Show Save & Cancel Button on clicking Edit Button
                $( "#saveuser" ).show();
                $( "#canceluser" ).show();
                $( "#deleteuser" ).show();

                //Enable all text boxes and switches on clicking Edit Button
                $( "#fname" ).prop( "disabled", false );
                $( "#lname" ).prop( "disabled", false );
                $( "#status" ).prop( "disabled", false );
                $( "#role" ).prop( "disabled", false );
                $( "#mobile" ).prop( "disabled", false );
                $( "#password" ).prop( "readonly", false );
            }

            //Redirects user to Users Page on clicking Cancel
            $('#canceluser').on('click', function(){
                window.location = "<?php echo base_url('Users'); ?>";
            });

            //Sweet Alert for Delete User
            $('#deleteuser').on('click', function(){
                swal({
                    dangerMode: true,
                    
                    icon: "warning",
                    title:"Heads Up!",
                    text:"Are you sure, do you want to delete this User Id?",
                    buttons: {
                        cancel: {
                            text:"Cancel",
                            value:"Cancel",
                            visible:true
                        },
                        confirm: {
                            text:"Yes",
                            value:"Yes",
                            visible:true
                        }
                    },
                    closeOnClickOutside: false,
                })
                .then((button)=>{
                    switch (button) {
                        case "Yes":
                            deleteuser();
                            break;
                        case "Cancel":
                            break;
                    }
                });
            });

            //Delete Function 
            function deleteuser(){
                $.ajax({  
                    url:"<?php echo base_url(); ?>Users/DeleteUser",   
                    contentType: false,  
                    cache: false,  
                    processData:false,  
                    success:function(data){
                        switch(data['error']){
                            case 2:
                                swal(data['errormsg']);
                                break;
                            case true:
                                swal({
                                    icon: "success",
                                    title:"Deleted",
                                    text:data['errormsg'],
                                    buttons: {
                                        confirm: {
                                            text:"Ok",
                                            value:"Ok",
                                            visible:true
                                        }
                                    },
                                    closeOnClickOutside: false,
                                })
                                .then((button)=>{
                                    switch (button) {
                                        case "Ok":
                                            window.location = "<?php echo base_url('Users'); ?>";
                                            break;
                                    }
                                });
                                break;
                        }
                        console.log(data);
                    },
                    fail:function(data){
                        console.log(data);
                    }
                });
            }
        });

        // Menu Trigger
        $('#menuToggle').on('click', function(event) {
            var windowWidth = $(window).width();         
            if (windowWidth<1010) { 
                $('body').removeClass('open'); 
                if (windowWidth<760){ 
                    $('#left-panel').slideToggle(); 
                } else {
                    $('#left-panel').toggleClass('open-menu');  
                } 
            } else {
                $('body').toggleClass('open');
                $('#left-panel').removeClass('open-menu');  
            } 
                 
        }); 

         
        $(".menu-item-has-children.dropdown").each(function() {
            $(this).on('click', function() {
                var $temp_text = $(this).children('.dropdown-toggle').html();
                $(this).children('.sub-menu').prepend('<li class="subtitle">' + $temp_text + '</li>'); 
            });
        });

        // Load Resize 
        $(window).on("load resize", function(event) { 
            var windowWidth = $(window).width();         
            if (windowWidth<1010) {
                $('body').addClass('small-device'); 
            } else {
                $('body').removeClass('small-device');  
            } 
            
        });

        $.noConflict();
                $(document).ready(function(){
            $("#SearchBox").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#SearchData tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });

        
        $(document).on('click', 'th', function() {
            var table = $(this).parents('table').eq(0);
            var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()));
            this.asc = !this.asc;
            if (!this.asc) {
                rows = rows.reverse();
            }
            table.children('tbody').empty().html(rows);
        });

        function comparer(index) {
            return function(a, b) {
                var valA = getCellValue(a, index),
                valB = getCellValue(b, index);
                return $.isNumeric(valA) && $.isNumeric(valB) ?
                valA - valB : valA.localeCompare(valB);
            };
        }

        function getCellValue(row, index) {
            return $(row).children('td').eq(index).text();
        }
    </script>
    
</body>
</html>
