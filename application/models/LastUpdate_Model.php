<?php 
    class LastUpdate_Model extends CI_Model{
    
        #Updates Last Updated status in DB
        public function LastUpdate(){
            $date = new \DateTime();
            $data = array(
               'lastupdate' => date_format($date, 'Y-m-d H:i:s')
            );
            $uid=$this->session->userdata('uid');
            if($uid!=''){
                $this->db->where('uid', $uid);
                $result=$this->db->update('users', $data);
                return $result;
            }else{
                return false;
            }
        }
    }
?>