<?php 
class Orders_Model extends CI_Model{

	#Function to get All Orders Content
    public function getAllOrders() {
        // $this->db->order_by('fdesc', 'ASC');
        $result = $this->db->get('orders');
        return $result;
    }

    #Funtction load to get order Data
    public function FetchOrder($pid) {
        $this->db->where('pid', $pid);
        $result = $this->db->get('orders');
        
        if($result->num_rows() == 1){
            
            #if order data is found
            $Order = array();
            
            $Order['pid']=$result->row(0)->pid;
            $Order['oid']=$result->row(0)->oid;
            $Order['pcatege']=$result->row(0)->pcatege;
            $Order['pprice']=$result->row(0)->pprice;
            $Order['pquantaty']=$result->row(0)->pquantaty;
            // $Order['psize']=$result->row(0)->psize;
            // $Order['pdiscount']=$result->row(0)->pdiscount;
            $Order['status']=$result->row(0)->status;

            return $Order;
        }else{
            
            #if Order data is not found
            return false;
        }
    }
}