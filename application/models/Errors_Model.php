<?php 
    class Errors_Model extends CI_Model{
        
        #Finds Error Message from DB
        public function FindErrorMsg($ecid){
            
            $result = $this->db->get_where('errorcodes', array('ecid' => $ecid), 1);
            $row = $result->row();

            if (isset($row)){
                $errormsg = $row->errormsg;
                return $errormsg;
            }else{
                return 'Unknown Error Occured';
            }
        }
    }
?>