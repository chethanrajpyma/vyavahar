<?php 
    class Users_Model extends CI_Model{
        
        #Checks if UserID already Exist in Database
        public function IsUserIDAlreadyExist($userid){
            $this->db->where('uid', $userid);
            $result = $this->db->get('users');
            if($result->num_rows() == 1){
                return 'Exist';
            }else{
                return 'NotExist';
            }
        }
        
        #Create New User ID
        public function CreateNewUser($NewUserData){
            
            #Security Cleaning - XSS Data Filtering 
            $NewUserData=$this->security->xss_clean($NewUserData);
            
            #Inserting into Table
            return $this->db->insert('users', $NewUserData);
        }
        
        #Fetches all Users List data
        public function FetchUserList($SField, $SOrder){
            $this->db->order_by($SField, $SOrder);
            $result = $this->db->get('users');
            return $result;
        }
        
        #Fetches particular users data
        public function FetchUserData($uid){
            $this->db->where('uid', $uid);
            $result = $this->db->get('users');
            
            if($result->num_rows() == 1){
                
                #if User data is found
                $UserData = array();
                
                $UserData['uid']=$result->row(0)->uid;
                $UserData['uid_index']=$result->row(0)->uid_index;
                $UserData['fname']=$result->row(0)->fname;
                $UserData['lname']=$result->row(0)->lname;
                $UserData['phone']=$result->row(0)->phone;
                $UserData['otp']=$result->row(0)->otp;
                $UserData['lastlogin']=$result->row(0)->lastlogin;
                $UserData['lastupdate']=$result->row(0)->lastupdate;
                $UserData['role']=$result->row(0)->role;
                $UserData['access']=$result->row(0)->access;
                $UserData['status']=$result->row(0)->status;
                $UserData['sign_file']=$result->row(0)->sign_file;
                
                $UserData['sessionid']=$result->row(0)->sessionid;
                
                return $UserData;
            }else{
                
                #if User data is not found
                return false;
            }
        }
        
        #Save Edited User Data.
        public function SaveEditedUser($NewUserIDData, $uid){
            
            #Security Cleaning - XSS Data Filtering
            $NewUserIDData=$this->security->xss_clean($NewUserIDData);

            $this->db->where('uid', $uid);
            return $this->db->update('users', $NewUserIDData);

        }
        
        #Delete User ID.
        public function DeleteUserID($uid){
            
            $this->db->where('uid', $uid);
            $result=$this->db->delete('users');
            return $result;
        }
    }
?>