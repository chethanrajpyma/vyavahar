<?php 
class Returns_Model extends CI_Model{

	#Function to get All return Content
    public function getAllReturns() {
        // $this->db->order_by('fdesc', 'ASC');
        $result = $this->db->get('orders');
        return $result;
    }

    #Funtction load to get order Data
    public function FetchReturns($oid) {
        $this->db->where('oid', $oid);
        $result = $this->db->get('orders');
        
        if($result->num_rows() == 1){
            
            #if order data is found
            $Returns = array();
            
            $Returns['pid']=$result->row(0)->pid;
            $Returns['oid']=$result->row(0)->oid;
            $Returns['pcatege']=$result->row(0)->pcatege;
            $Returns['pprice']=$result->row(0)->pprice;
            $Returns['pquantaty']=$result->row(0)->pquantaty;
            // $Returns['psize']=$result->row(0)->psize;
            // $Returns['pdiscount']=$result->row(0)->pdiscount;
            $Returns['status']=$result->row(0)->status;

            return $Returns;
        }else{
            
            #if Order data is not found
            return false;
        }
    }

}
