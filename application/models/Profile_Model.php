<?php 
    class Profile_Model extends CI_Model{
        
        #Updates changelog Table
        public function GetUserDetails($uid){
            
            #Security Cleaning - XSS Data Filtering 
            $uid=$this->security->xss_clean($uid);
            
            $result = $this->db->get_where('users', array('uid' => $uid), 1);
            $row = $result->row();

            if (isset($row)){
                $UserData = array(
                    'uid' => $this->session->userdata('uid'),
                    'fname' => $row->fname,
                    'lname' => $row->lname,
                    'phone' => $row->phone,
                    'role' => $row->role,
                    'access' => $row->access,
                    'status' => $row->status,
                    'sign_file' => $row->sign_file
                );
                return $UserData;
            }else{
                return 'Unknown';
            }
        }
        
        #Changes the Login Password
        public function change_login_password($CurrentPassword, $NewPassword){
            
            #Load Login Model
            $this->load->model('Login_Model');
            
            #Encrypt Password
            $EncPass = $this->Login_Model->EncPass($CurrentPassword);
            $NewEncPass = $this->Login_Model->EncPass($NewPassword);
            
            #Get User ID from Sessions
            $uid=$this->session->userdata('uid');
            
            #Fetch Login User Current Password
            $result = $this->db->get_where('users', array('uid' => $uid), 1);
            $row = $result->row();

            if (isset($row)){
                $UserData = array(
                    'pwd' => $row->pwd
                );
                if($UserData['pwd']===$EncPass){
                    
                    #If Old Password is Correct
                    $NewPwd = array(
                        'pwd' => $NewEncPass
                    );
                    
                    #Update New Password in DB
                    $this->db->where('uid', $uid);
                    $result=$this->db->update('users', $NewPwd);
                    return 'Changed';
                }else{
                    
                    #If Old Password is Wrong
                    return 'Not Changed';
                }
            }else{
                
                #If DB Error Occurs
                return 'DB Error';
            }
        }

    }
?>