<?php 
class Seller_Model extends CI_Model{

    #Function to get All stae
    public function getState() {
        $this->db->order_by('statename', 'ASC');
        $result = $this->db->get('state');
        return $result;
    }

    #Function to get  SellerMaster
    public function GetSellerMaster() {
        $this->db->order_by('sellername', 'ASC');
        $result = $this->db->get('sellers');
        return $result;
    }

    #Ajax call retreives all the table data.
    public function IfSellerMasterAlreadyExists($cname){

    	#Security Cleaning - XSS Data Filtering 
        $role=$this->security->xss_clean($role);

        $this->db->where('compname', $cname);
        $result = $this->db->get('sellers');
        if($result->num_rows() == 1){
            return 'Exist';
        }else{
            return 'NotExist';
        }
    }

    #Create SellerMaster
    public function SavePR($NewData){
        
        #Security Cleaning - XSS Data Filtering 
        $NewData=$this->security->xss_clean($NewData);
        
        #Inserting into Table
        return $this->db->insert('sellers', $NewData);
    }

    #Funtction load to FetchSellerMaster details
    public function FetchSellerMaster($sid){
        $this->db->where('sid', $sid);
        $result = $this->db->get('sellers');
        
        if($result->num_rows() == 1){
            
            #if Seller data is found
            $SellerData = array();
            
            $SellerData['sid']=$result->row(0)->sid;
            $SellerData['sellername']=$result->row(0)->sellername;
            $SellerData['compname']=$result->row(0)->compname;
            $SellerData['cnum']=$result->row(0)->cnum;
            $SellerData['scategory']=$result->row(0)->scategory;
            $SellerData['status']=$result->row(0)->status;
            
            return $SellerData;
        }else{
            
            #if SellerData is not found
            return false;
        }
    }

    #Save Edited Seller Data.
    public function UpdateSellerMaster($NewData, $ID){
        
        $NewData=$this->security->xss_clean($NewData);
        
        $this->db->where('sid', $ID);
        $result = $this->db->update('sellers',$NewData);
        return $result;
    }
// --------------------------------------------------------------------------------------------------------------------------------//
    #Create New Products
    public function AddNewProduct($NewData){
        
        #Security Cleaning - XSS Data Filtering 
        $NewData=$this->security->xss_clean($NewData);
        
        #Inserting into Table
        return $this->db->insert('products', $NewData);
    }

    #Function to check name already exists.
    public function IfProductAlreadyExists($title){
        $this->db->where('pname', $title);
        $result = $this->db->get('products');
        if($result->num_rows() == 1){
            return 'Exist';
        }else{
            return 'NotExist';
        }
    }

    #Ajax call retreives all the table data.
   public function GetaAllProduct($role){
       
        #Security Cleaning - XSS Data Filtering 
        $role=$this->security->xss_clean($role);

        $this->db->where('pcatege', $role);
        $results = $this->db->get('products');
        return $resultsAry = $results->result_array();
    }
}


