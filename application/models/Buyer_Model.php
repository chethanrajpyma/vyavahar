<?php 
class Buyer_Model extends CI_Model{

	#Function to get All Products Content
    public function getAllProducts() {
        // $this->db->order_by('fdesc', 'ASC');
        $result = $this->db->get('buyer');
        return $result;
    }

    #Function to check products  already exists.
    public function IfProductsAlreadyExists($pname) {
        $this->db->where('fname', $pname);
        $result = $this->db->get('buyer');
        if($result->num_rows() == 1){
            return 'Exist';
        }else{
            return 'NotExist';
        }
    }

     #Function Save Products
    public function SaveProducts($NewData) {
        
        #Security Cleaning - XSS Data Filtering 
        $NewData=$this->security->xss_clean($NewData);
        
        #Inserting into Table
        return $this->db->insert('buyer', $NewData);
    }

    #Funtction load to get products Data
    public function FetchProduct($pid) {
        $this->db->where('id', $pid);
        $result = $this->db->get('buyer');
        
        if($result->num_rows() == 1){
            
            #if products data is found
            $Product = array();
            
            $Product['fname']=$result->row(0)->fname;
            $Product['lname']=$result->row(0)->lname;
         

            return $Product;
        }else{
            
            #if products data is not found
            return false;
        }
    }

     # funtction Product Update  Data.
    public function ProductsUpdate($NewData, $ID){
        
        $NewData=$this->security->xss_clean($NewData);

        $this->db->where('id', $ID);
        $result = $this->db->update('buyer',$NewData);
        return $result;
    }
}