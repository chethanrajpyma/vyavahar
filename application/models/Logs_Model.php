<?php 
    class Logs_Model extends CI_Model{
        
        #Updates changelog Table
        public function UpdateLog($clcid, $details){
            
            $result = $this->db->get_where('changelogcodes', array('clcid' => $clcid), 1);
            $row = $result->row();

            if (isset($row)){
                $logmsg = $row->logmsg;
                $logcat = $row->logcat;
                $ip = $this->input->ip_address();
                $date = new \DateTime();
                $LogData = array(
                    'date' => date_format($date, 'Y-m-d H:i:s'),
                    'uid' => $this->session->userdata('uid'),
                    'logmsg' => $logmsg,
                    'details' => "$details",
                    'logcat' => $logcat,
                    'ip' => $ip
                );
                $this->db->insert('changelog', $LogData);
            }else{
                return 'Unknown';
            }
        }
        
        #Updates changelog Table
        public function UpdateForcedLog($uid, $clcid, $details){
            
            $result = $this->db->get_where('changelogcodes', array('clcid' => $clcid), 1);
            $row = $result->row();

            if (isset($row)){
                $logmsg = $row->logmsg;
                $logcat = $row->logcat;
                $ip = $this->input->ip_address();
                $date = new \DateTime();
                $LogData = array(
                    'date' => date_format($date, 'Y-m-d H:i:s'),
                    'uid' => $uid,
                    'logmsg' => $logmsg,
                    'details' => $details,
                    'logcat' => $logcat,
                    'ip' => $ip
                );
                $this->db->insert('changelog', $LogData);
            }else{
                return 'Unknown';
            }
        }
        
        #Fetches Log Info from changelog Table
        public function DisplayLog($clcid,$logmsg,$date){
            
            $role = $this->session->userdata('role');
            $uid = $this->session->userdata('uid');
            $this->db->order_by($clcid, $logmsg); 
            
            if($date!=''){
                #Displays only for Specific Date
                $this->db->like('date', $date); 
            }else{
                #Displays Logs for 14 Days + Today
                $date = new DateTime('-14 day');
                $this->db->where('date >', $date->format('Y-m-d')); 
            }
            
            if($role!='SuperAdmin'){
                #Displays Logs only for Specific User ID if Not SuperAdmin
                $this->db->where('uid', $uid); 
            }
            
            $result = $this->db->get('changelog');
            return $result;
        }
        
        #Cron to delete logs older than 14 days
        public function CronDeleteLogs1Day(){
            $result = $this->db->query("DELETE FROM `changelog` WHERE date < CURRENT_DATE - INTERVAL 15 DAY");
        }
    }
?>