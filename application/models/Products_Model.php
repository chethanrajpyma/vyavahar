<?php 
class Products_Model extends CI_Model{

	#Function to get All Products Content
    public function getAllProducts() {
        // $this->db->order_by('fdesc', 'ASC');
        $result = $this->db->get('products');
        return $result;
    }

    #Function to check products  already exists.
    public function IfProductsAlreadyExists($pname) {
        $this->db->where('pname', $pname);
        $result = $this->db->get('products');
        if($result->num_rows() == 1){
            return 'Exist';
        }else{
            return 'NotExist';
        }
    }

     #Function Save Products
    public function SaveProducts($NewData) {
        
        #Security Cleaning - XSS Data Filtering 
        $NewData=$this->security->xss_clean($NewData);
        
        #Inserting into Table
        return $this->db->insert('products', $NewData);
    }

    #Funtction load to get products Data
    public function FetchProduct($pid) {
        $this->db->where('pid', $pid);
        $result = $this->db->get('products');
        
        if($result->num_rows() == 1){
            
            #if products data is found
            $Product = array();
            
            $Product['pname']=$result->row(0)->pname;
            $Product['pcatege']=$result->row(0)->pcatege;
            $Product['pdescrip']=$result->row(0)->pdescrip;
            $Product['pprice']=$result->row(0)->pprice;
            $Product['pquantaty']=$result->row(0)->pquantaty;
            $Product['psize']=$result->row(0)->psize;
            $Product['pdiscount']=$result->row(0)->pdiscount;
            $Product['status']=$result->row(0)->status;

            return $Product;
        }else{
            
            #if products data is not found
            return false;
        }
    }

     # funtction Product Update  Data.
    public function ProductsUpdate($NewData, $ID){
        
        $NewData=$this->security->xss_clean($NewData);

        $this->db->where('pid', $ID);
        $result = $this->db->update('products',$NewData);
        return $result;
    }
}