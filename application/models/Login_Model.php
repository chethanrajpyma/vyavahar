<?php 
    class Login_Model extends CI_Model{
        
        #Finds User Data if UID and Password are Correct
        public function login_user($userid, $password){
            
            $EncPass=$this->Login_Model->EncPass($password);
            
            $this->db->where('uid', $userid);
            $this->db->where('pwd', $EncPass);

            $result = $this->db->get('users');
            
            if($result->num_rows() == 1){
                
                $UserData = array();
                
                $UserData['uid']=$result->row(0)->uid;
                $UserData['fname']=$result->row(0)->fname;
                $UserData['lname']=$result->row(0)->lname;
                $UserData['phone']=$result->row(0)->phone;
                $UserData['otp']=$result->row(0)->otp;
                $UserData['lastlogin']=$result->row(0)->lastlogin;
                $UserData['lastupdate']=$result->row(0)->lastupdate;
                $UserData['role']=$result->row(0)->role;
                $UserData['access']=$result->row(0)->access;
                $UserData['status']=$result->row(0)->status;
                
                $UserData['sessionid']=$result->row(0)->sessionid;
                
                return $UserData;
            }else{
                return false;
            }
        }
        
        #Encrypts the Password
        public function EncPass($EnteredPass){
            $Salt = 'ij3920';
            $SaltedPassword = $EnteredPass;
            $SaltedPassword.= $Salt;
            $EncPassword = hash('tiger128,3', $SaltedPassword);
            return $EncPassword;
        }
        
        #Updates Login DateTime Stamp & LoggedIn Status
        public function Login(){
            $date = new \DateTime();
            
            $data = array(
                'lastlogin' => date_format($date, 'Y-m-d H:i:s'),
                'sessionid' => session_id()
            );
            $uid=$this->session->userdata('uid');
            if($uid!=''){
                
                 #Security Cleaning - XSS Data Filtering
                $data=$this->security->xss_clean($data);
                
                $this->db->where('uid', $uid);
                $result=$this->db->update('users', $data);
                return $result;
            }else{
                return false;
            }
        }
        
        #Updates Logout Status on Logout
        public function Logout(){
            $data = array(
                'sessionid' => ''
            );
            $uid=$this->session->userdata('uid');
            if($uid!=''){
                
                #Security Cleaning - XSS Data Filtering
                $data=$this->security->xss_clean($data);
                
                $this->db->where('uid', $uid);
                $result=$this->db->update('users', $data);
                return $result;
            }else{
                return false;
            }
        }   
            
        #Finds if category is permitted or not. Compars access string with module name
        public function isPermitted($Module, $AccessString){
            $pos = strpos($AccessString, $Module);

            if ($pos === false) {
                return false;
            } else {
                return true;
            }
        }
        
        #Sets Sessionid value in DB to Null to force logout
        public function ForcedLogout($uid){
            $data = array(
                'sessionid' => ''
            );
            
            #Security Cleaning - XSS Data Filtering
            $data=$this->security->xss_clean($data);
            
            $this->db->where('uid', $uid);
            $result=$this->db->update('users', $data);
            return $result;
        }
        
        #Get dynamic session id from database
        public function GetSessionID($userid){
            
            $this->db->where('uid', $userid);

            $result = $this->db->get('users');
            
            if($result->num_rows() == 1){
                
                $UserData = array();
                
                $UserData['sessionid']=$result->row(0)->sessionid;
                $UserData['sessionstatus']='Fetched';
                
                return $UserData;
            }else{
                $UserData['sessionstatus']='NotFetched';
                return $UserData;
            }
        }
        
        #Check wwether the same uid and password logged in any other machine.
        public function CheckDynamicLoginStatus(){
            $this->load->model('Login_Model');
            $this->load->model('Logs_Model');
            $this->load->model('Errors_Model');

            $DynamicSessionID=$this->Login_Model->GetSessionID($this->session->userdata('uid'));
            if($DynamicSessionID['sessionstatus']=='Fetched'){
                if($DynamicSessionID['sessionid']!=session_id()){

                    if($DynamicSessionID['sessionid']=='Logout by Cron'){
                        
                        #If logout by cron
                        $ValidationErrors=array(
                            'errors'=>$this->Errors_Model->FindErrorMsg('11')
                        );

                        $this->session->set_flashdata($ValidationErrors);
                        redirect('Login');
                    }else{
                        
                        #If Forced Logout Attempted
                        $ValidationErrors=array(
                            'errors'=>$this->Errors_Model->FindErrorMsg('12')
                        );

                        $this->session->set_flashdata($ValidationErrors);
                        redirect('Login');
                    }
                }
            }else{
                $ValidationErrors=array(
                   'errors'=>$this->Errors_Model->FindErrorMsg('13')
                    // 'errors'=>''
                );

                $this->session->set_flashdata($ValidationErrors);
                redirect('Login');
            }
            return TRUE;
        }
        #Cron to logout all user ids which are idle for more than 1 hour
        public function CronAutoLogout1Hr(){

            $result = $this->db->get('users');
            $Today = abs(strtotime(date('Y-m-d h:m:s')));
            foreach($result->result() as $row){
                
                $LastUpdate=abs(strtotime($row->lastupdate));
                $IdleSeconds = $Today-$LastUpdate;
                $IdleMinutes = $IdleSeconds/60;
                $IdleMinutesRounded = floor($IdleMinutes);
                
                if($IdleMinutesRounded>60){
                    $data=array(
                        'sessionid'=>'Logout by Cron'
                    );
                    $this->db->where('uid', $row->uid);
                    $result=$this->db->update('users', $data);
                }
            }
        }
        
        #Resets Cron logout
        public function ResetLogoutByCron($uid){

            $data=array(
                'sessionid'=>''
            );
            $this->db->where('uid', $uid);
            $result=$this->db->update('users', $data);
            
        }
    }
?>