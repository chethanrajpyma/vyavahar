<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    #Default Function
	public function index(){
        
        $SField='uid';
        $SOrder='asc';
        
        #Header Information
        $data['title']='Users List';
        
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $this->load->library('form_validation');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        
        #Fetch all Users Details
        $this->load->model('Users_Model');
        $data['result']=$this->Users_Model->FetchUserList($SField, $SOrder);
        
        $this->load->view('users', $data);
	}

    #Function to load view to add new user
    public function Add() {

        #Header Information
        $data['title']='Add New User';
        
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        // $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();

        #Fetch all Users Details
        $this->load->model('Users_Model');
        // $results['result']=$this->Users_Model->FetchUserList();

        $this->load->view('users_add', $data);    
    }
    
    #Function to create and store new user details in database
    public function CreateUser() {
        
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        // $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        
        $this->load->model('Logs_Model');
        $this->load->model('Errors_Model');
        $this->load->model('Users_Model');
        
        #Return Output in JSON Format
        header('Content-Type: application/json');

        #Customizing Error Style
        $this->form_validation->set_error_delimiters('<span>', '</span>');
        
        #Display Error Msg from DB
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|max_length[16]|alpha_numeric_spaces',
        
             array('required' => "Enter First Name",
                   'max_length' => "Enter less than 16 characters",
                   'alpha_numeric_spaces' => "Enter only a-z, A-Z, 0-9 and spaces"));   
        
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|max_length[16]|alpha_numeric_spaces',
        
             array('required' => "Enter Last Name",
                   'max_length' => "Enter less than 16 characters",
                   'alpha_numeric_spaces' => "Enter only a-z, A-Z, 0-9 and spaces"));
        
        $this->form_validation->set_rules('userid', 'User ID', 'required|max_length[63]|valid_email', 
              array('valid_email' => "User ID should be in email@example.com Format",
                    'max_length' => "Enter less than 63 characters",
                    'required' => "Enter User ID"));
        
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[63]|min_length[8]|alpha_numeric_spaces', 
              array('alpha_numeric_spaces' => "Enter only a-z, A-Z, 0-9 and spaces",
                    'max_length' => "Enter less than 63 characters",
                    'min_length' => "Password should be atleast 8 Characters",
                    'required' => "Enter Password"));
        
        $this->form_validation->set_rules('role', 'Role', 'required|alpha_numeric_spaces', 
              array('alpha_numeric_spaces' => "Enter only a-z, A-Z, 0-9 and spaces",
                    'required' => "Select Role"));
        
        $this->form_validation->set_rules('mobile', 'Mobile', 'required|min_length[10]|max_length[10]|numeric', 
              array('numeric' => "Enter valid mobile number",
                    'max_length' => "Enter ten digit mobile number in 9XXXXXXXXX format",
                    'min_length' => "Enter ten digit mobile number in 9XXXXXXXXX format",
                    'required' => "Enter Phone Number"));
        
        #Set flashdata to be used in page reload on validation error
        $flashuserdata=array(
            'fdfname' => $this->input->post('fname'),
            'fdlname' => $this->input->post('lname'),
            'fduid' => $this->input->post('userid'),
            'fdpwd' => $this->input->post('password'),
            'fdphone' => $this->input->post('mobile'),
            'fdrole' => $this->input->post('role'),
            'dflag' => 'Yes'
        );
        $this->session->set_flashdata($flashuserdata);
        
        #Run Form Validations
        if($this->form_validation->run()== FALSE ){
            
            $ValidationErrors=array(
                'error_fname'=>form_error('fname'),
                'error_lname'=>form_error('lname'),
                'error_userid'=>form_error('userid'),
                'error_password'=>form_error('password'),
                'error_role'=>form_error('role'),
                'error_mobile'=>form_error('mobile'),
                'errors'=>'<div class="alert alert-warning">'."Enter mandatory values".'</div>'
            );
            
            $this->session->set_flashdata($ValidationErrors);
            redirect(base_url('Users/Add'));
        }else{ 
            
            #If No Form Errors, Proceeding to create a new user id
            
            #Check if user id already exist in Database
            if($this->Users_Model->IsUserIDAlreadyExist($this->input->post('userid'))=='Exist'){
                $ValidationErrors=array(
                    'errors'=>"User ID already exists. Please choose different ID"
                );
                
                $this->session->set_flashdata($ValidationErrors);
                redirect(base_url('Users/Add'));

            }else{

                #Create New User ID Data Array
                $NewUserIDData=array(
                    'fname' => $this->input->post('fname'),
                    'lname' => $this->input->post('lname'),
                    'uid' => $this->input->post('userid'),
                    'pwd' => $this->Login_Model->EncPass($this->input->post('password')),
                    'phone' => $this->input->post('mobile'),
                    'otp' => "No",
                    'role' => "SuperAdmin",
                    'access' => "Null",
                    'status' => 'Active'
                );

                #Call function from Model to create new user. The function returns 1 if new User ID is created successfully
                if($this->Users_Model->CreateNewUser($NewUserIDData)){
                    
                    //$ValidationErrors['errors']=1;

                    $this->load->model('Users_Model');

                    $ValidationErrors['errors']='<div class="alert alert-success">'."New User ID created successfully".'</div>';
                    $this->session->set_flashdata($ValidationErrors);
                    // $newData = $this->Users_Model->getLicenceInfo($this->session->userdata('cid'));
                    // $this->session->set_userdata($newData);
                    redirect(base_url('Users'));
                }
            }
        }
    }

    #Function to save edited user details
    public function Edit() {
        
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();

        #Capture UID from Url
        $selecteduid =  $this->uri->segment(3);
        
        // #Redirect to Users Page if not a valid email. 
        if(!valid_email($selecteduid)){
            redirect(base_url('Users'));
        }        

        #Fetch Users Details
        $this->load->model('Users_Model');
        $this->load->model('Login_Model');
        $this->load->library('form_validation');
        $data['UserData']=$this->Users_Model->FetchUserData($selecteduid);
        
        #Setting session for user id to edit.This will be used in Model
        $uid=array(
            'edituid'=> $data['UserData']['uid']
        );

        $this->session->set_userdata($uid);

        #Header Information
        $data['title']='Edit User';

        $this->load->view('users_edit',$data);
        
        #Update Last Update DateTime Stamp in cust_users Table
        $this->load->model('LastUpdate_Model');
        $this->LastUpdate_Model->LastUpdate();

    }    

    #Edits an existing user id with new values
    public function EditUser(){

        $this->load->model('Logs_Model');
        $this->load->model('Errors_Model');
        $this->load->model('Users_Model');
        $this->load->model('Login_Model');
        
        #Return Output in JSON Format
        header('Content-Type: application/json');
        
        #Fecthing uid from session variable
        $uid=$this->session->userdata('edituid');

        #Customizing Error Style
        $this->form_validation->set_error_delimiters('<span>', '</span>');
     
        #Display Error Msg from DB
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|max_length[16]|alpha_numeric_spaces',
        
             array('required' => $this->Errors_Model->FindErrorMsg('28'),
                   'max_length' => $this->Errors_Model->FindErrorMsg('29'),
                   'alpha_numeric_spaces' => $this->Errors_Model->FindErrorMsg('30')));   
        
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|max_length[16]|alpha_numeric_spaces',
        
             array('required' => $this->Errors_Model->FindErrorMsg('32'),
                   'max_length' => $this->Errors_Model->FindErrorMsg('31'),
                   'alpha_numeric_spaces' => $this->Errors_Model->FindErrorMsg('30')));
        
         $this->form_validation->set_rules('password', 'Password', 'max_length[63]|min_length[8]|alpha_numeric_spaces', 
              array('alpha_numeric_spaces' => $this->Errors_Model->FindErrorMsg('30'),
                    'max_length' => $this->Errors_Model->FindErrorMsg('29'),
                    'min_length' => $this->Errors_Model->FindErrorMsg('20'),
                    'required' => $this->Errors_Model->FindErrorMsg('2')));
        
        $this->form_validation->set_rules('mobile', 'Mobile', 'required|min_length[10]|max_length[10]|numeric', 
              array('numeric' => $this->Errors_Model->FindErrorMsg('33'),
                    'max_length' => $this->Errors_Model->FindErrorMsg('34'),
                    'min_length' => $this->Errors_Model->FindErrorMsg('34'),
                    'required' => $this->Errors_Model->FindErrorMsg('35')));
        
        
        #Set flashdata to be used in page reload on validation error
        $flashuserdata=array(
            'fdfname' => $this->input->post('fname'),
            'fdlname' => $this->input->post('lname'),
            'fdphone' => $this->input->post('mobile'),
            'fdrole' => $this->input->post('role'),
            'fdstatus' => $this->input->post('status'),
            'dflag' => 'Yes'
        );
        $this->session->set_flashdata($flashuserdata);
        
        #Run Form Validations
        if($this->form_validation->run()== FALSE){
            
            $ValidationErrors=array(
                'error_fname'=>form_error('fname'),
                'error_lname'=>form_error('lname'),
                'error_password'=>form_error('password'),
                'error_mobile'=>form_error('mobile'),
                'errors'=> '<div class="alert alert-danger">'.$this->Errors_Model->FindErrorMsg('37').'</div>'
            );
            
            #Set session to hold the edit mode if any error is there after submitting.
            $editmode=array(
                'editmode'=> 'True'
            );
            $this->session->set_flashdata($editmode);
            
            $this->session->set_flashdata($ValidationErrors);
            redirect(base_url('Users/Edit/').$uid);
        }else{ 

            #Create New User ID Data Array
            $NewUserIDData=array(
                'fname' => $this->input->post('fname'),
                'lname' => $this->input->post('lname'),
                'uid' => $uid,
                'phone' => $this->input->post('mobile'),
                'otp' => "No",
                'role' => "SuperAdmin",
                'access' => "Null",
                'status' => $this->input->post('status')
            );

            if($this->input->post('password')!=''){
                $NewUserIDData['pwd']=$this->Login_Model->EncPass($this->input->post('password'));
            }

            #Call function from Model to create new user. The function returns 1 if new User ID is created successfully
            if($this->Users_Model->SaveEditedUser($NewUserIDData, $uid)){

                #Updating Log
                $LogResult = $this->Logs_Model->UpdateLog('8', $uid);
                #Send JSON Result with Form Success Message
                $ValidationErrors['result']='<div class="alert alert-success">'.$this->Errors_Model->FindErrorMsg('36').'</div>';
                $this->session->set_flashdata($ValidationErrors);
                redirect(base_url('Users/Edit/').$uid);
            }
        }

        #Update Last Update DateTime Stamp in cust_users Table
        $this->load->model('LastUpdate_Model');
        $this->LastUpdate_Model->LastUpdate();
    }
    
    #Function to Delete user
    public function DeleteUser(){
        $this->load->model('Logs_Model');
        $this->load->model('Errors_Model');
        $this->load->model('Login_Model');
        $this->load->model('Users_Model');
        
        #Return Output in JSON Format
        header('Content-Type: application/json');
        
        $uid=$this->session->userdata('edituid');
        $loggedin_uid=$this->session->userdata('uid');
        
        if($uid===$loggedin_uid){
            //if Logged In User ID and Deleting User ID are same, reject delete request
            $result=array(
                'error'=>2,
                'errormsg'=>$this->Errors_Model->FindErrorMsg('107')
            );
        }else{

            //if Deleting User ID is SuperAdmin Login, Reject delete request
            $AccountDetails = $this->Users_Model->FetchUserData($uid);

            if($AccountDetails['role']=='SuperAdmin'){
                $result=array(
                    'errors'=>'<div class="alert alert-danger">'.$this->Errors_Model->FindErrorMsg('105').'</div>'
                ); 
            }else{
                //if Logged In User ID and Deleting User ID are not same, delete user id
                $result=array(
                    'error'=>$this->Users_Model->DeleteUserID($uid),
                    'errormsg'=>$this->Errors_Model->FindErrorMsg('106')
                );
            
                #Updating Log
                $LogResult = $this->Logs_Model->UpdateLog('013', $uid);
            }
        }
        
        #Update Last Update DateTime Stamp in cust_users Table
        $this->load->model('LastUpdate_Model');
        $this->LastUpdate_Model->LastUpdate();
        
        $JSON_Result=json_encode($result, JSON_PRETTY_PRINT);
        echo $JSON_Result;
    }
}
