<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sellers extends CI_Controller {

#Function load to view  Sellers form
    public function Sellers() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Seller Details";
        $this->load->view('seller', $data);
    }
}