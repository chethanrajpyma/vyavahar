<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    #Default Function
	public function index() {

        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        
        #Header Information
        $data['title']='Seller Profile Details';
        
        $this->load->model('Profile_Model');
        $uid=$this->session->userdata('uid');
        if($uid!=''){
            $UserDetails = $this->Profile_Model->GetUserDetails($uid);
            
            $data['u_uid']=$UserDetails['uid'];
            $data['u_fname']=$UserDetails['fname'];
            $data['u_lname']=$UserDetails['lname'];
            $data['u_phone']=$UserDetails['phone'];
            $data['u_access']=$UserDetails['access'];
            $data['u_role']=$UserDetails['role'];
            $data['u_status']=$UserDetails['status'];
            $data['u_sign_file']=$UserDetails['sign_file'];
        }
        
        #Restricting the Page Access if No Access Rights
        $this->load->view('profile', $data);
        #Update Last Update DateTime Stamp in cust_users Table
        $this->load->model('LastUpdate_Model');
        $this->LastUpdate_Model->LastUpdate();
        
	}

    #Function to change Login Password
    public function ChangeLoginPassword() {

        #Finding Forced Logout Status
         $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        
        #Load Errors & Logs Models
        $this->load->model('Errors_Model');
        $this->load->model('Logs_Model');
        
        #Return Output in JSON Format
        header('Content-Type: application/json');
        
        #Customizing Error Style
        $this->form_validation->set_error_delimiters('<span>', '</span>');
        
        #Display Error Msg from DB
        $this->form_validation->set_rules('CurrentPassword', 'Current Password', 'required',
             array('required' => $this->Errors_Model->FindErrorMsg('24')));
        
        $this->form_validation->set_rules('NewPassword', 'New Password', 'required|min_length[8]|alpha_numeric_spaces', 
             array('required' => $this->Errors_Model->FindErrorMsg('18'),
                   'min_length' => $this->Errors_Model->FindErrorMsg('20'),
                   'alpha_numeric_spaces' => $this->Errors_Model->FindErrorMsg('21')));
        
        $this->form_validation->set_rules('ConfirmPassword', 'Confirm Password', 'required|matches[NewPassword]', 
             array('required' => $this->Errors_Model->FindErrorMsg('19'),
                   'matches' => $this->Errors_Model->FindErrorMsg('22')));
        
        #Run Form Validations
        if($this->form_validation->run()== FALSE) {
            
            #Setting Form Validation Errors
            $ValidationErrors=array(
                'error_currentpassword'=>form_error('CurrentPassword'),
                'error_newpassword'=>form_error('NewPassword'),
                'error_confirmpassword'=>form_error('ConfirmPassword'),
                'result'=>'<div class="alert alert-warning">Resove Invalid Entries!</div>'
            );
            
           $this->session->set_flashdata($ValidationErrors);

           redirect(base_url('Profile'));

        } else {

            #If No Form Errors, Proceeding to authenticate
            $Result=array(
                'error_currentpassword'=>'',
                'error_newpassword'=>'',
                'error_confirmpassword'=>''
            );
            
            #Capturing Post Values of the Form Fields
            $CurrentPassword = $this->input->post('CurrentPassword');
            $NewPassword = $this->input->post('NewPassword');  
            $ConfirmPassword = $this->input->post('ConfirmPassword');  
            
            #Get User Data from DB
            $this->load->model('Profile_Model');
            $UserData=$this->Profile_Model->change_login_password($CurrentPassword, $NewPassword);
            
            #Password Changed Successfully
            if($UserData=='Changed') {
                $Result['result']='<div class="alert alert-success">'.$this->Errors_Model->FindErrorMsg("25").'</div>';
                $LogResult = $this->Logs_Model->UpdateLog('5', 'Self');
                $Result['result_code']='Success';

            }else{  

                #Current Password did not matched. New password not changed
                if($UserData=='Not Changed') {
                     $Result['result']='<div class="alert alert-danger">'.$this->Errors_Model->FindErrorMsg("26").'</div>';
                    $LogResult = $this->Logs_Model->UpdateLog('6', 'Self');
                    $Result['result_code']='Fail';

                }else{ 

                    #Database Error
                    if($UserData=='DB Error') {
                        $Result['result']='<div class="alert alert-danger">'."Database Error".'</div>';
                        $Result['result_code']='Fail';
                    } 
                }
            }
            
           $this->session->set_flashdata($Result);
           redirect(base_url('Profile'));
        }
    }
}