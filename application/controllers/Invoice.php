<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {
	
	 public function index() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Invoice Bills";
        $this->load->view('invoice', $data);
    }	
}
