<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Seller extends CI_Controller {


    public function index() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Sellers List";
        #Function load to get state list
        $this->load->model('Seller_Model');
        $data['State']=$this->Seller_Model->getState();

        $this->load->view('seller-list', $data);
    }

    #Function load to view SellerMaster 
    public function SellerMaster() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Seller List";
        #Function load to get state list
        $this->load->model('Seller_Model');
        $data['Seller']=$this->Seller_Model->GetSellerMaster();

        $this->load->view('seller', $data);
    }

     #Function to retrieve SellerMaster on Ajax call
    public function GetSellerMaster() {

        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        
        #Stop the script if Input Request is not from Ajax
        if(!$this->input->is_ajax_request()) {
            redirect(base_url().'Dashboard');
        }
        
        $role = $this->uri->segment(3);
        
        $this->load->model('Seller_Model');

        //passsing the uri data to the model.
        $data = $this->SellerModel->GetAllSellerMaster($role);
        
        //set the table rows.
        $stringtbl['rows'] = '<tr></tr>';
        $stringtbl['rowcount'] = 0;
        $count = 1;
        $stringtbl['rows'] = "<tr class='visible-xs visible-sm'>
                                    <td style='cursor:pointer;' ><b class='pull-right'>ID</b></td>
                                <td style='cursor:pointer;'><b>Name</b></td>
                                <td style='cursor:pointer;'><b>Country</b></td>
                                <td></td>
                                </tr>";
        if($data){
            foreach($data as $row) {

                //set the Centers ID parameter to sort it easily.
                $sid     = $count++; 
                if($sid <=9){$sid   ='000'.$sid ;}
                if($sid >9 && $sid  <=99){$sid  ='00'.$sid  ;}
                if($sid >99 && $sid <=999){$sid ='0'.$sid   ;}
                
                $stringtbl['rows'] .= "<tr>
                                    <td>".$sid  ."</td>
                                    <td>".$row['sellername']."</td>
                                    <td>".$row['compname']."</td>
                                    <td style='text-align: center;'><a href='Edit SellerMaster Details/".$row['sid    ']."'><span class='fa fa-edit' data-toggle='tooltip' title='Edit' data-placement='left'></span></a></td>
                                    </tr>";
                
                $stringtbl['rowcount'] = $stringtbl['rowcount'] + 1;
            }
        }else{
            $stringtbl['rows'] = "<tr>
                                    <td colspan='4' style='text-align:center;font-weight:700;'>Record Not Found</td>
                                    </tr>";
        }

        #Return Output in JSON Format
        header('Content-Type: application/json');
        
        $JSON_Result=json_encode($stringtbl, JSON_PRETTY_PRINT);
        echo $JSON_Result;
    }

    #Ajax call retreives all the table data.
    public function GetAllSellerMaster($role){
       
        #Security Cleaning - XSS Data Filtering 
        $role=$this->security->xss_clean($role);

        $this->db->where('sellername', $role);
        $results = $this->db->get('sellers');
        return $resultsAry = $results->result_array();
    }

    #Function load to view AddSellerMaster 
    public function AddSellerMaster() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();

        $data['title'] = "Add Seller  Details  ";
        $this->load->view('seller-add', $data);
    }

    #Function load to save Seller Master
    public function SaveSellerMaster() {

        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();

        $this->load->model('Errors_Model');
        $this->load->model('Seller_Model');

        #Return Output in JSON Format
        header('Content-Type: application/json');

        #Customizing Error Style
        $this->form_validation->set_error_delimiters('<span>', '</span>');

        #Display Error Msg from DB
        $this->form_validation->set_rules('cname', 'Company Name', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('119')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('sname', 'Name of the Seller', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('120')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('buissness', 'Buissness Established', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('126')));      

        #Display Error Msg from DB
        $this->form_validation->set_rules('phone', 'Contact Numbers', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('60')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('email', 'Email', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('127')));  

        #Display Error Msg from DB
        $this->form_validation->set_rules('pcateg', 'Category', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('61'))); 

        #Display Error Msg from DB
        $this->form_validation->set_rules('address1', 'Address1', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('70')));  

        #Display Error Msg from DB
        $this->form_validation->set_rules('address2', 'Address2', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('70')));  

        #Display Error Msg from DB
        $this->form_validation->set_rules('city', 'City', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('55')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('state', 'State', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('56')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('country', 'Country', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('57')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('pincode', 'Pincode', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('58')));  

        #Display Error Msg from DB
        if (empty($_FILES['TImage']['name'])) {
            $this->form_validation->set_rules('TImage', 'Image',array('required' => $this->Errors_Model->FindErrorMsg('110')));
        }
        // #Display Error Msg from DB
        // if (empty($_FILES['SImage']['name'])) {
        //     $this->form_validation->set_rules('SImage', 'Image' ,array('required' => $this->Errors_Model->FindErrorMsg('110')));
        // }

        #Session Flashdata
        $fdata = array(
            'fcname' => $this->input->post('cname'),
            'fsname' => $this->input->post('sname'),
            'fbuissness' => $this->input->post('buissness'),
            'fphone' => $this->input->post('phone'),
            'fmobile' => $this->input->post('mobile'),
            'femail' => $this->input->post('email'),
            'fpcateg' => $this->input->post('pcateg'),
            'faddress1' => $this->input->post('address1'),
            'faddress2' => $this->input->post('address2'),
            'fcity' => $this->input->post('city'),
            'fstate' => $this->input->post('state'),
            'fcountry' => $this->input->post('country'),
            'fpincode' => $this->input->post('pincode')
        );

        $this->session->set_flashdata($fdata);

        #Run Form Validations
        if($this->form_validation->run()== FALSE){
            
            $ValidationErrors=array(
                'error_cname'=>form_error('cname'),
                'error_sname'=>form_error('sname'),
                'error_buissness'=>form_error('buissness'),
                'error_phone'=>form_error('phone'),
                'error_mobile'=>form_error('mobile'),
                'error_email'=>form_error('email'),
                'error_pcateg'=>form_error('pcateg'),
                'error_address1'=>form_error('address1'),
                'error_address2'=>form_error('address2'),
                'error_city'=>form_error('city'),
                'error_state'=>form_error('state'),
                'error_country'=>form_error('country'),
                'error_pincode'=>form_error('pincode'),
                'error_TImage'=>form_error('TImage'),
                // 'error_MImage'=>form_error('MImage'),
                'errors'=>'<div class="alert alert-danger">'.$this->Errors_Model->FindErrorMsg('37').'</div>'
            );
            
            $this->session->set_flashdata($ValidationErrors);
            redirect(base_url('Seller/AddSellerMaster'));
        }else{

            #Check if Seller Master name already exists
            if($this->Seller_Model->IfSellerMasterAlreadyExists($this->input->post('cname'))=='Exist' ){
                $ValidationErrors=array(
                    'error_cname'=> '<p style="color:red;">'.$this->Errors_Model->FindErrorMsg('122').'</p>'
                );

                $this->session->set_flashdata($ValidationErrors);
                redirect(base_url('Seller/AddSellerMaster'));
            }

            #Creating First File Name
            $FirstImage=$this->input->post('TImage');
            
            $FirstImage = str_replace("@", "-", $FirstImage);
            $FirstImage = str_replace(".", "-", $FirstImage);

            #File Validation & Upload Rules
            $config['upload_path']          = 'assets/images/products/';
            $config['allowed_types']        = 'jpg|png|jpeg|gif';
            $config['file_name']            = $FirstImage;
            $config['overwrite']            = TRUE;
            $config['max_size']             = 2048;
            $config['max_width']            = 0;
            $config['max_height']           = 0;

            #Initiate File Validation to Upload
            $this->upload->initialize($config);
            $this->load->library('upload', $config);

            if(!$this->upload->do_upload('TImage', FALSE)){

                #File Upload Fail
                $First = array('error_info' => $this->upload->display_errors());
                $First['resultsignature']='<div class="alert alert-danger">'."Image Upload Failed".'</div>';
                
                if($_FILES['TImage']['error'] != 4)
                {
                    return redirect(base_url('Seller/SaveSellerMaster'));
                }
            }          

            #First File Upload Success
            $First = array('success_info' => $this->upload->data());

            $First['result_code']='Success';
            
            $FirstFile = $First['success_info']['file_name'];

            #Creating Second File Name
            $SecondImage=$this->input->post('MImage');
            
            $SecondImage = str_replace("@", "-", $SecondImage);
            $SecondImage = str_replace(".", "-", $SecondImage);

            #File Validation & Upload Rules
            $config['upload_path']          = 'assets/images/products/';
            $config['allowed_types']        = 'jpg|png|jpeg|gif';
            $config['file_name']            = $SecondImage;
            $config['overwrite']            = TRUE;
            $config['max_size']             = 2048;
            $config['max_width']            = 0;
            $config['max_height']           = 0;

            #Initiate File Validation to Upload
            $this->upload->initialize($config);
            $this->load->library('upload', $config);

            if(!$this->upload->do_upload('MImage', FALSE)){

                #File Upload Fail
                $Second = array('error_info' => $this->upload->display_errors());
                $Second['resultsignature']='<div class="alert alert-danger">'."Image Upload Failed".'</div>';
                
                if($_FILES['MImage']['error'] != 4)
                {
                    return redirect(base_url('Seller/SaveSellerMaster'));
                }
            }          

            #First File Upload Success
            $Second = array('success_info' => $this->upload->data());

            $Second['result_code']='Success';
            
            $SecondFile = $Second['success_info']['file_name'];

            $NewData = array(
                'compname' => $this->input->post('cname'),
                'sellername' => $this->input->post('sname'),
                'buissestablish' => $this->input->post('buissness'),
                'phone' => $this->input->post('phone'),
                'cnum' => $this->input->post('mobile'),
                'semail' => $this->input->post('email'),
                'scategory' => $this->input->post('pcateg'),
                'address1' => $this->input->post('address1'),
                'address2' => $this->input->post('address2'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'pin' => $this->input->post('pincode'),
                 'timage' => $FirstFile,
                 // 'mimage' => $SecondFile,
                'status' => "Active"


            );

            echo "<pre>";
            echo $NewData;
            exit;

            #Add new Feedback
            $result=$this->Seller_Model->SavePR($NewData);

            #If Seller is added successfully.
            if($result){
                
                $ValidationErrors['errors']='<div class="alert alert-success">'.$this->Errors_Model->FindErrorMsg('123').'</div>';
                $this->session->set_flashdata($ValidationErrors);
                
                redirect(base_url('Seller/SellerMaster'));
            }
        }
    }

    #Function load to view Edit SellerMaster form
    public function EditSellerMaster() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Edit Seller Master";
        $sid =  $this->uri->segment(3);

        #Set Session to used while saving editede details
        $Flashid=array(
            'sid'=>$sid
        );

        $this->session->set_flashdata($Flashid);

        $this->load->model('Seller_Model');
        $data['SellerData']=$this->Seller_Model->FetchSellerMaster($sid);
        #Function load to view
        $this->load->view('seller-edit', $data);
    }

// -------------------------------------------------------------------------------------------------------------------------------//
    #Function load to view SellerMaster 
    public function Products() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = " Products Added";

        $this->load->view('productsadded', $data);
        // $this->load->view('published-books', $data);
    }

    #Function to retrieve Products on Ajax call
    public function GetAddedProducts() {

        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        
        #Stop the script if Input Request is not from Ajax
        if(!$this->input->is_ajax_request()) {
            redirect(base_url().'Dashboard');
        }
        
        $role = $this->uri->segment(3);
        
        $this->load->model('Seller_Model');

        //passsing the uri data to the model.
        $data = $this->Seller_Model->GetaAllProduct($role);
        
        //set the table rows.
        $stringtbl['rows'] = '<tr></tr>';
        $stringtbl['rowcount'] = 0;
        $count = 1;
        $stringtbl['rows'] = "<tr class='visible-xs visible-sm'>
                                    <td style='cursor:pointer;' ><b class='pull-right'>ID</b></td>
                                <td style='cursor:pointer;'><b>Product Name</b></td>
                                <td style='cursor:pointer;'><b>Product Category</b></td>
                                <td style='cursor:pointer;'><b>Added By</b></td>
                                <td style='cursor:pointer;'><b>Status</b></td>
                                <td></td>
                                </tr>";
        if($data){
            foreach($data as $row) {

                //set the Product ID parameter to sort it easily.
                $pid = $count++; 
                if($pid<=9){$pid='000'.$pid;}
                if($pid>9 && $pid<=99){$pid='00'.$pid;}
                if($pid>99 && $pid<=999){$pid='0'.$pid;}
                
                $stringtbl['rows'] .= "<tr>
                                    <td>".$pid."</td>
                                    <td>".$row['pname']."</td>
                                    <td>".$cat."</td>
                                    <td>".$row['bstatus']."</td>
                                    <td style='text-align: center;'><a href='Seller/EditAddedProduct/".$row['pid']."'><span class='fa fa-edit' data-toggle='tooltip' title='Edit' data-placement='left'></span></a></td>
                                    </tr>";
                
                $stringtbl['rowcount'] = $stringtbl['rowcount'] + 1;
            }
        }else{
            $stringtbl['rows'] = '';
        }

        #Return Output in JSON Format
        header('Content-Type: application/json');
        
        $JSON_Result=json_encode($stringtbl, JSON_PRETTY_PRINT);
        echo $JSON_Result;
    }

    #Function load to view published book form
    public function CreateProductsAdded() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Add Productss";
        $this->load->view('products-add', $data);
    }

    #Function load to save published book form
    public function SaveNewProducts() {

        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();

        $this->load->model('Errors_Model');
        $this->load->model('Seller_Model');

        #Return Output in JSON Format
        header('Content-Type: application/json');

        #Customizing Error Style
        $this->form_validation->set_error_delimiters('<span>', '</span>');

        #Display Error Msg from DB
        $this->form_validation->set_rules('prodtcateg', 'Product Category', 'trim|required|max_length[512]|min_length[6]',
            array('required' => $this->Errors_Model->FindErrorMsg('38')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('prodtname', 'Product Name', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('39')));

        // #Display Error Msg from DB
        // if (empty($_FILES['Signature']['name'])) {
        //     $this->form_validation->set_rules('Signature', 'Image', 'required',array('required' => $this->Errors_Model->FindErrorMsg('40')));
        // }

        #Store Department name to Session Flashdata
        $fdata = array(
            'fprodtcateg' => $this->input->post('prodtcateg'),
            'fprodtname' => $this->input->post('prodtname')
        );

        $this->session->set_flashdata($fdata);

        #Run Form Validations
        if($this->form_validation->run()== FALSE){
            
            $ValidationErrors=array(
                'fprodtcateg'=>form_error('prodtcateg'),
                'error_prodtname'=>form_error('prodtname'),
                // 'error_fimage'=>form_error('Signature'),
                'errors'=>'<div class="alert alert-danger">'.$this->Errors_Model->FindErrorMsg('37').'</div>'
            );
            
            $this->session->set_flashdata($ValidationErrors);
            redirect(base_url('Products/CreateProductsAdded'));
        }else{

            #Creating New File Name
            $NewFileName=$this->input->post('Signature');
            
            $NewFileName = str_replace("@", "-", $NewFileName);
            $NewFileName = str_replace(".", "-", $NewFileName);

            #File Validation & Upload Rules
            $config['upload_path']          = 'assets/images/books/';
            $config['allowed_types']        = 'jpg|png|jpeg|gif';
            $config['file_name']            = $NewFileName;
            $config['overwrite']            = TRUE;
            $config['max_size']             = 1024;
            $config['max_width']            = 0;
            $config['max_height']           = 0;

            #Initiate File Validation to Upload
            $this->upload->initialize($config);
            $this->load->library('upload', $config);

            if(!$this->upload->do_upload('Signature', FALSE)){

                #File Upload Fail
                $Result = array('error_info' => $this->upload->display_errors());
                $Result['resultsignature']='<div class="alert alert-danger">'."Image Upload Failed".'</div>';
                
                if($_FILES['Signature']['error'] != 4)
                {
                    return redirect(base_url('Seller/CreateProductsAdded'));
                }
            }

            #Check if Product name already exists
            if($this->Seller_Model->IfProductAlreadyExists($this->input->post('pname'))=='Exist' ){
                $ValidationErrors=array(
                    'error_fmaster'=> '<p style="color:red;">'.$this->Errors_Model->FindErrorMsg('41').'</p>'
                );

                $this->session->set_flashdata($ValidationErrors);
                redirect(base_url('Seller/CreateProductsAdded'));
            }

            #File Upload Success
            $Result = array('success_info' => $this->upload->data());

            $Result['result_code']='Success';
            
            $NewFile = $Result['success_info']['file_name'];

            $NewData = array(
                'pcatege' => $this->input->post('role'),
                'pname' => $this->input->post('title'),
                // 'pprice' => $this->input->post('title'),
                // 'psize' => $this->input->post('title'),
                // 'pcatelog' => $this->input->post('title'),
                // 'pid' => $this->input->post('title'),
                // 'pseller' => $this->input->post('title'),
                // 'pdescrip' => $this->input->post('title'),
                // 'bimage' => $NewFile,
                'status' => "Active"
            );

            #Add new Product
            $result=$this->Seller_Model->AddNewProduct($NewData);

            #If Product is added successfully.
            if($result){
                
                $ValidationErrors['errors']='<div class="alert alert-success">'.$this->Errors_Model->FindErrorMsg('42').'</div>';
                $this->session->set_flashdata($ValidationErrors);
                #Updating Log
                $result = $this->Logs_Model->UpdateLog('9', '-');
                redirect(base_url('Seller/Products'));
            }
        }
    }

     #Funtction load to get books details
    public function FetchProductsData($pid){
        $this->db->where('pid', $pid);
        $result = $this->db->get('products');
        
        if($result->num_rows() == 1){
            
            #if User data is found
            $ProductsData = array();
            
            $ProductsData['cat']=$result->row(0)->cat;
            $ProductsData['btitle']=$result->row(0)->btitle;
            $ProductsData['bimage']=$result->row(0)->bimage;
            $ProductsData['bstatus']=$result->row(0)->bstatus;
            
            return $ProductsData;
        }else{
            
            #if Quote data is not found
            return false;
        }
    }

     



}
