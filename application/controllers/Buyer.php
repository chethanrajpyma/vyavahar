<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buyer extends CI_Controller {

    public function index() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "All Buyers";

        #Function load to get Products-Content list
        $this->load->model('Buyer_Model');
        $data['EditProducts']=$this->Buyer_Model->getAllProducts();    
		//print_r($data); die();
        $this->load->view('buyer', $data);
    }

     #Function load to view AddProducts Us Forms
    public function AddBuyer() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Add New Buyer";   
        $this->load->view('buyer-add', $data);
    }

     #Function load to save Products form
    public function SaveNewBuyer() {

        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();

        $this->load->model('Errors_Model');
        $this->load->model('Buyer_Model');

        #Return Output in JSON Format
        header('Content-Type: application/json');

        #Customizing Error Style
        $this->form_validation->set_error_delimiters('<span>', '</span>');

        #Display Error Msg from DB
        $this->form_validation->set_rules('fname', 'Buyer First Name', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('200')));

       
        #Buyer data to Session Flashdata
        $fdata = array(
            'fname' => $this->input->post('fname'),
            'lname' => $this->input->post('lname')
        );

        $this->session->set_flashdata($fdata);

        #Run Form Validations
        if($this->form_validation->run()== FALSE){
            
            $ValidationErrors=array(
                'error_fname'=>form_error('fname'),
                'error_lname'=>form_error('lname'),
                'errors'=>'<div class="alert alert-danger">'.$this->Errors_Model->FindErrorMsg('37').'</div>'
            );  
            
            $this->session->set_flashdata($ValidationErrors);
            redirect(base_url('Buyer/AddBuyer'));
        }else{

            #Check if Products  already exists
            if($this->Buyer_Model->IfProductsAlreadyExists($this->input->post('fname'))=='Exist' ){
                $ValidationErrors=array(
                    'error_pname'=> '<p style="color:red;">'.$this->Errors_Model->FindErrorMsg('142').'</p>'
                );

                $this->session->set_flashdata($ValidationErrors);
                redirect(base_url('Buyer/AddBuyer'));
            }

            $NewData = array(
                'fname' => $this->input->post('fname'),
				'lname' => $this->input->post('lname'),
                'status' => "Active"
            );

            #Add new products
            $result=$this->Buyer_Model->SaveProducts($NewData);

            #If products added successfully.
            if($result){
                
                $ValidationErrors['errors']='<div class="alert alert-success">'.$this->Errors_Model->FindErrorMsg('143').'</div>';
                $this->session->set_flashdata($ValidationErrors);
                #Updating Log
                $result = $this->Logs_Model->UpdateLog('31', 'Buyer Added Successfully');
                redirect(base_url('Buyer/UpdateBuyer'));
            }
        }
    }

    #Function load to view EditProducts form
    public function EditBuyer() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();

        // $data['title'] = "EditProducts";
        $PID =  $this->uri->segment(3);

        #Set Session to used while saving edited details
        $Flashid=array(
            'id'=>$PID
        );

        #Header Information
        $data['title']="Edit Buyer Details";

        $this->session->set_flashdata($Flashid);
        #Function load to get Products Data
        $this->load->model('Buyer_Model');
        $data['Product']=$this->Buyer_Model->FetchProduct($PID);
        #Function load to view
        $this->load->view('buyer-edit', $data);
    }

    #Function load to update Products form
    public function UpdateBuyer() {

        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();

        $this->load->model('Errors_Model');
        $this->load->model('Buyer_Model');

        #Return Output in JSON Format
        header('Content-Type: application/json');

        #Customizing Error Style
        $this->form_validation->set_error_delimiters('<span>', '</span>');

        #Display Error Msg from DB
        $this->form_validation->set_rules('fname', 'Buyer First Name', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('130')));

		#Display Error Msg from DB
        $this->form_validation->set_rules('lname', 'Buyer Last Name', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('130')));

	

        $ID=$this->session->flashdata('id');
		//echo($ID); die();
        #Product data to Session Flashdata
        $fdata = array(
            'fname' => $this->input->post('fname'),
            'lname' => $this->input->post('lname')
        );

        $this->session->set_flashdata($fdata);

        #Run Form Validations
        if($this->form_validation->run()== FALSE){
            
            $ValidationErrors=array(
                'fname' => $this->input->post('fname'),
                'lname' => $this->input->post('lname'),
                'errors'=>'<div class="alert alert-danger">'.$this->Errors_Model->FindErrorMsg('37').'</div>'
            );
            
            $this->session->set_flashdata($ValidationErrors);
            redirect(base_url('Buyer'));   
        }else{

            #Session User ID
            $Created=$this->session->userdata('id');

            $NewData = array(
                'fname' => $this->input->post('fname'),
                'lname' => $this->input->post('lname'),
                'status' => "Active"
            );

            #Update Products data
            $result=$this->Buyer_Model->ProductsUpdate($NewData, $ID);

            #If Products is updated successfully.
            if($result){
                
                $ValidationErrors['errors']='<div class="alert alert-success">'.$this->Errors_Model->FindErrorMsg('144').'</div>';
                $this->session->set_flashdata($ValidationErrors);
                #Updating Log
                $result = $this->Logs_Model->UpdateLog('32','Buyer Updated successfully');
                redirect(base_url('Buyer'));  
            }
        }   
    }  
}