<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {
	
    public function index() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Order Details";

        #Function load to get Products-Content list
        $this->load->model('Orders_Model');
        $data['EditOrders']=$this->Orders_Model->getAllOrders();

        $this->load->view('orders', $data);
    }

    #Function load to view EditOrders form
    public function EditOrders() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();

        // $data['title'] = "EditOrders";
        $OID =  $this->uri->segment(3);

        #Set Session to used while saving edited details
        $Flashid=array(
            'oid'=>$OID
        );

        #Header Information
        $data['title']="Edit Order Details";

        $this->session->set_flashdata($Flashid);
        #Function load to get Products Data
        $this->load->model('Orders_Model');
        $data['EditOrders']=$this->Orders_Model->FetchOrder($OID);
        #Function load to view
        $this->load->view('orders-edit', $data);
    }

   
}