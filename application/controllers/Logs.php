<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends CI_Controller {
	
	 public function index() {	
	 	
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Users Logs";
        #Function load to get Logs
        $this->load->model('Logs_Model');
        // $data['LogsData']=$this->Logs_Model->DisplayLog($clcid,$logmsg,$date);

        #Function load to get logs view
        $this->load->view('logs', $data);
    }	
}

 	