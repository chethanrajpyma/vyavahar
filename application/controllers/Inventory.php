<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {

    public function index() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Inventory Details";

        #Function load to get Products-Content list
        $this->load->model('Inventory_Model');
        $data['EditInventory']=$this->Inventory_Model->getAllInventory();

        $this->load->view('inventory', $data);
    }
}