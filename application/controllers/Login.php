<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    #Default Function
    public function index() {

        $this->load->helper('url');
        $this->load->view('login');
    }

    #Logins User
    public function Login(){

        $this->load->model('Logs_Model');
        $this->load->model('Errors_Model');

        $flashuid = $this->input->post('userid');  
        $flashpwd = $this->input->post('password');
        
        //Set Flash UID and PWD to retain UID and PWD in Login Form
        $ValidationErrors=array(
            'flashuid'=>$flashuid,
            'flashpwd'=>$flashpwd
        );

        $this->session->set_flashdata($ValidationErrors);
        
        #Customizing Error Style
        $this->form_validation->set_error_delimiters('<span>', '</span>');
        
        #Display Error Msg from DB
        $this->form_validation->set_rules('userid', 'User ID', 'trim|valid_email|required',
             array('valid_email' => $this->Errors_Model->FindErrorMsg('3'),
                   'required' => $this->Errors_Model->FindErrorMsg('1')));
        
        $this->form_validation->set_rules('password', 'Password', 'required', 
             array('required' => $this->Errors_Model->FindErrorMsg('2')));
        
        #Run Form Validations
        if($this->form_validation->run()== FALSE){
            
            $ValidationErrors=array(
                'error_userid'=>form_error('userid'),
                'error_password'=>form_error('password')
            );
            
            #Setting Flash Data with Form Validation Errors
            $this->session->set_flashdata($ValidationErrors);
            redirect('Login');
        }
        else{ 
            
            #If No Form Errors, Proceeding to authenticate
            $password = $this->input->post('password');
            $userid = $this->input->post('userid');  
            
            #Get User Data from DB
            $this->load->model('Login_Model');
            $UserData=$this->Login_Model->login_user($userid, $password);
            
            if($UserData){ 
                
                #If user id status is 'Active'
                if($UserData['status']=='Active'){
                    
                    if($UserData['sessionid']=='Logout by Cron'){
                        
                        #If Forced Logout Attempted
                        $result=$this->Login_Model->ResetLogoutByCron($UserData['uid']);
                        
                        $ValidationErrors=array(
                            'errors'=>$this->Errors_Model->FindErrorMsg('6')
                        );

                        $this->session->set_flashdata($ValidationErrors);
                        redirect('Login');
                        
                    }
                    
                    #Check the user if logged in on another system
                    if($UserData['sessionid']=='' || $UserData['sessionid']===session_id()){
                        
                        #If User Data is found on DB. Set Session Data
                        $this->session->set_userdata($UserData);

                        $result=$this->Login_Model->Login();

                        #Updating Log
                        $LogResult = $this->Logs_Model->UpdateLog('1', '-');
                        redirect('Dashboard');
                    }else{
                        
                        #If User is Logged In On another system
                        $ValidationErrors=array(
                            'errors'=>'<div class="alert alert-warning">'."You are already Logged In on Another System.".' <a href="'.base_url('Login/ForcedLogout').'">Click</a> to Logout from Other Device/s'.'</div>'
                        );
                        $this->session->set_flashdata($ValidationErrors);
                        
                        #Set Session Data for User ID alone to find Forced Login
                        $RawUserData['rawuid']=$UserData['uid'];
                        $RawUserData['rawotp']=$UserData['otp'];
                        $RawUserData['rawmobile']=$UserData['phone'];
                        $this->session->set_userdata($RawUserData);
                        
                        redirect('Login');
                    }
                }else{ 
                    
                    #If user id status is 'Inactive'
                    if($UserData['status']=='Inactive'){
                    
                        #If User Status is Inactive
                        $ValidationErrors=array(
                            'errors'=>'<div class="alert alert-danger">'.$this->Errors_Model->FindErrorMsg('7').'</div>'
                        );

                        $this->session->set_flashdata($ValidationErrors);
                        redirect('Login');
                    }else{
                        
                        #If user id status is 'Leaver'
                        if($UserData['status']=='Leaver'){

                            #If User Status is Leaver
                            $ValidationErrors=array(
                                'errors'=>'<div class="alert alert-danger">'.$this->Errors_Model->FindErrorMsg('8').'</div>'
                            );

                            $this->session->set_flashdata($ValidationErrors);
                            redirect('Login');
                        }
                    }
                } 
            }else{
                #If User Credentials are Invalid
                $ValidationErrors=array(
                    'errors'=>'<div class="alert alert-danger">'.$this->Errors_Model->FindErrorMsg('4').'</div>'
                );

                $this->session->set_flashdata($ValidationErrors);
                redirect('Login');
            }
        }
    }

    #Logouts the User. Sets 'Loginstatus' to 'LoggedOut'
    public function Logout(){
        $this->load->model('Login_Model');
        $this->load->model('Logs_Model');
        
        $result=$this->Login_Model->Logout();
        
        $LogResult = $this->Logs_Model->UpdateLog('2', '-');
        
        $this->session->sess_destroy();
        redirect('Login');
    }
    
    #Logout the user forcely
    public function ForcedLogout(){
        $this->load->model('Login_Model');
        $this->load->model('Logs_Model');
        $this->load->model('Errors_Model');
        
        #Check OTP Status. If user has opted for OTP, redirect him to OTP
        if($this->session->userdata('rawotp')=='Yes'){
            redirect('VerifyOTP');
            
        }else{
            
            #If user has not opted for OTP, forced logout him from other device
            $result=$this->Login_Model->ForcedLogout($this->session->userdata('rawuid'));

            if($result){
                
                #If User id is logged out successfully
                $ValidationErrors=array(
                    'errors'=>'<div class="alert alert-success">'.$this->Errors_Model->FindErrorMsg('9').'</div>'
                );
                $LogResult = $this->Logs_Model->UpdateForcedLog($this->session->userdata('rawuid'), '3', '-');
            }else{
                
                #If user id is unable to logout, display error message : Unable to logout
                $ValidationErrors=array(
                    'errors'=>'<div class="alert alert-success">'.$this->Errors_Model->FindErrorMsg('10').'</div>'
                );
            }
            $this->session->set_flashdata($ValidationErrors);
            redirect('Login');
        }
    }
}
