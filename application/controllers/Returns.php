<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Returns extends CI_Controller {

	   public function index() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Return Details";

        #Function load to get Products-Content list
        $this->load->model('Returns_Model');
        $data['EditReturns']=$this->Returns_Model->getAllReturns();

        $this->load->view('returns', $data);
    }

}