<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    public function index() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Products Added";

        #Function load to get Products-Content list
        $this->load->model('Products_Model');
        $data['EditProducts']=$this->Products_Model->getAllProducts();

        $this->load->view('products', $data);
    }

     #Function load to view AddProducts Us Forms
    public function AddProducts() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();
        $data['title'] = "Add New Products";
        $this->load->view('products-add', $data);
    }

     #Function load to save Products form
    public function SaveNewProducts() {

        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();

        $this->load->model('Errors_Model');
        $this->load->model('Products_Model');

        #Return Output in JSON Format
        header('Content-Type: application/json');

        #Customizing Error Style
        $this->form_validation->set_error_delimiters('<span>', '</span>');

        #Display Error Msg from DB
        $this->form_validation->set_rules('pname', 'Products Name', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('200')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('pcateg', 'Products Category', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('201')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('descp', 'Products Description', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('202')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('pprice', ' Products Price', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('203')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('quantaty', ' Products Quantaty', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('204')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('psize', ' Products Size', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('205')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('discount', ' Discount Price', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('206')));

        // #Display Error Msg from DB
        // $this->form_validation->set_rules('discount', ' Discount Price', 'trim|required',
        //     array('required' => $this->Errors_Model->FindErrorMsg('206')));

        #Display Error Msg from DB
        // $this->form_validation->set_rules('Bimg', ' Banner Image', 'trim|required',
        //     array('required' => $this->Errors_Model->FindErrorMsg('137')));

        #Display Error Msg from DB
        // $this->form_validation->set_rules('Timg', ' Thumb Image', 'trim|required',
        //     array('required' => $this->Errors_Model->FindErrorMsg('138')));

        #Display Error Msg from DB
        // $this->form_validation->set_rules('Mimg', ' Main Image', 'trim|required',
        //     array('required' => $this->Errors_Model->FindErrorMsg('139')));


        #Products data to Session Flashdata
        $fdata = array(
            'fpname' => $this->input->post('pname'),
            'fpcateg' => $this->input->post('pcateg'),
            'fpdesc' => $this->input->post('descp'),
            'fpprice' => $this->input->post('pprice'),
            'fquantaty' => $this->input->post('quantaty'),
            'fpsize' => $this->input->post('psize'),
            'fdiscount' => $this->input->post('discount')
        );

        $this->session->set_flashdata($fdata);

        #Run Form Validations
        if($this->form_validation->run()== FALSE){
            
            $ValidationErrors=array(
                'error_pname'=>form_error('pname'),
                'error_pcateg'=>form_error('pcateg'),
                'error_fdescp'=>form_error('descp'),
                'error_pprice'=>form_error('pprice'),
                'error_quantaty'=>form_error('quantaty'),
                'error_psize'=>form_error('psize'),
                'error_discount'=>form_error('discount'),
                'errors'=>'<div class="alert alert-danger">'.$this->Errors_Model->FindErrorMsg('37').'</div>'
            );  
            
            $this->session->set_flashdata($ValidationErrors);
            redirect(base_url('Products/AddProducts'));
        }else{

            #Check if Products  already exists
            if($this->Products_Model->IfProductsAlreadyExists($this->input->post('pname'))=='Exist' ){
                $ValidationErrors=array(
                    'error_pname'=> '<p style="color:red;">'.$this->Errors_Model->FindErrorMsg('142').'</p>'
                );

                $this->session->set_flashdata($ValidationErrors);
                redirect(base_url('Products/AddProducts'));
            }

            $NewData = array(
                'pname' => $this->input->post('pname'),
                'pcatege' => $this->input->post('pcateg'),
                'pdescrip' => $this->input->post('descp'),
                'pprice' => $this->input->post('pprice'),
                'pquantaty' => $this->input->post('quantaty'),
                'psize' => $this->input->post('psize'),
                'pdiscount' => $this->input->post('discount'),
                'status' => "Active"
            );

            #Add new products
            $result=$this->Products_Model->SaveProducts($NewData);

            #If products added successfully.
            if($result){
                
                $ValidationErrors['errors']='<div class="alert alert-success">'.$this->Errors_Model->FindErrorMsg('143').'</div>';
                $this->session->set_flashdata($ValidationErrors);
                #Updating Log
                $result = $this->Logs_Model->UpdateLog('31', 'Product Added Successfully');
                redirect(base_url('Products/UpdateProducts'));
            }
        }
    }

    #Function load to view EditProducts form
    public function EditProducts() {
        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();

        // $data['title'] = "EditProducts";
        $PID =  $this->uri->segment(3);

        #Set Session to used while saving edited details
        $Flashid=array(
            'pid'=>$PID
        );

        #Header Information
        $data['title']="Edit Products Details";

        $this->session->set_flashdata($Flashid);
        #Function load to get Products Data
        $this->load->model('Products_Model');
        $data['Product']=$this->Products_Model->FetchProduct($PID);
        #Function load to view
        $this->load->view('products-edit', $data);
    }

    #Function load to update Products form
    public function UpdateProducts() {

        #Finding Forced Logout Status
        $this->load->model('Login_Model');
        $DynamicLoginStatus=$this->Login_Model->CheckDynamicLoginStatus();

        $this->load->model('Errors_Model');
        $this->load->model('Products_Model');

        #Return Output in JSON Format
        header('Content-Type: application/json');

        #Customizing Error Style
        $this->form_validation->set_error_delimiters('<span>', '</span>');

        #Display Error Msg from DB
        $this->form_validation->set_rules('pname', 'Product Name', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('130')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('pcateg', 'Product Category', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('131')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('descp', 'Product Description', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('48')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('pprice', ' Product Price', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('134')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('quantaty', ' Product Quantaty', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('135')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('psize', ' Product Size', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('135')));

        #Display Error Msg from DB
        $this->form_validation->set_rules('discount', ' Product Discount', 'trim|required',
            array('required' => $this->Errors_Model->FindErrorMsg('135')));

        // #Display Error Msg from DB
        // $this->form_validation->set_rules('Bimg', ' Banner Image', 'trim|required',
        //     array('required' => $this->Errors_Model->FindErrorMsg('137')));

        #Display Error Msg from DB
        // $this->form_validation->set_rules('Timg', ' Thumb Image', 'trim|required',
        //     array('required' => $this->Errors_Model->FindErrorMsg('138')));

        #Display Error Msg from DB
        // $this->form_validation->set_rules('Mimg', ' Main Image', 'trim|required',
        //     array('required' => $this->Errors_Model->FindErrorMsg('139')));
      

        $ID=$this->session->flashdata('pid');

        #Product data to Session Flashdata
        $fdata = array(
            'fpname' => $this->input->post('pname'),
            'fpcateg' => $this->input->post('pcateg'),
            'fpdesc' => $this->input->post('descp'),
            'fpprice' => $this->input->post('pprice'),
            'fquantaty' => $this->input->post('quantaty'),
            'fpsize' => $this->input->post('psize'),
            'fdiscount' => $this->input->post('discount')
        );

        $this->session->set_flashdata($fdata);

        #Run Form Validations
        if($this->form_validation->run()== FALSE){
            
            $ValidationErrors=array(
                'pname' => $this->input->post('pname'),
                'pcatege' => $this->input->post('pcateg'),
                'pdescrip' => $this->input->post('descp'),
                'pprice' => $this->input->post('pprice'),
                'pquantaty' => $this->input->post('quantaty'),
                'psize' => $this->input->post('psize'),
                'pdiscount' => $this->input->post('discount'),
                'errors'=>'<div class="alert alert-danger">'.$this->Errors_Model->FindErrorMsg('37').'</div>'
            );
            
            $this->session->set_flashdata($ValidationErrors);
            redirect(base_url('Products/EditProducts/'.$ID));
        }else{

            #Session User ID
            $Created=$this->session->userdata('pid');

            $NewData = array(
                'pname' => $this->input->post('pname'),
                'pcatege' => $this->input->post('pcateg'),
                'pdescrip' => $this->input->post('descp'),
                'pprice' => $this->input->post('pprice'),
                'pquantaty' => $this->input->post('quantaty'),
                'psize' => $this->input->post('psize'),
                'pdiscount' => $this->input->post('discount'),
                'status' => "Active"
            );

            #Update Products data
            $result=$this->Products_Model->ProductsUpdate($NewData, $ID);

            #If Products is updated successfully.
            if($result){
                
                $ValidationErrors['errors']='<div class="alert alert-success">'.$this->Errors_Model->FindErrorMsg('144').'</div>';
                $this->session->set_flashdata($ValidationErrors);
                #Updating Log
                $result = $this->Logs_Model->UpdateLog('32','Products Updated successfully');
                redirect(base_url('Products'));
            }
        }   
    }  
}