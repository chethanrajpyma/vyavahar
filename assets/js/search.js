$(document).ready(function() {
    //Search Box
    $("#SearchBox").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#SearchData tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});