$('#CommonTable').DataTable( {
     "paging":   false,
     "searching": false,
     "info": false,
     "order": [[ 0, "desc" ]],
     "columnDefs": [
        { "type": "num", "targets": 0 }
      ]
} );